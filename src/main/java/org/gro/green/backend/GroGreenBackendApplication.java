package org.gro.green.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroGreenBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroGreenBackendApplication.class, args);
	}

}
