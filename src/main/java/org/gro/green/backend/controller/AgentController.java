package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.dto.AgentDto;
import org.gro.green.backend.service.AgentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AgentController {

    @Autowired
    AgentService agentService;

    @GetMapping("/agents/all")
    public ResponseEntity<List<Agent>> getAllAgents() {
        List<Agent> allAgents;
        try {
            allAgents = agentService.getAllAgents();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allAgents);
    }

    @GetMapping("/agents/all/{status}")
    public ResponseEntity<List<Agent>> getAllAgentsByStatus(@PathVariable("status") String status) {
        List<Agent> allAgents;
        try {
            allAgents = agentService.getAllAgentsByStatus(status);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allAgents);
    }
    @GetMapping("/agents/user/{userId}")
    public ResponseEntity<Agent> getAgentByUserId(@PathVariable("userId") String userId) {
        Agent agent;
        try {
            agent = agentService.getAgentByUserId(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(agent);
    }

    @GetMapping("/agents/{agentId}")
    public ResponseEntity<Agent> getAgentById(@PathVariable("agentId") String agentId) {
        Agent agent;
        try {
            agent = agentService.getAgentByAgentId(agentId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(agent);
    }

    @PostMapping("/agents/register")
    public ResponseEntity<Agent> addAgent(@Valid @RequestBody AgentDto agentDto) {
        Agent newAgent;
        try {
            newAgent = agentService.registerAgent(agentDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(newAgent);
    }

    @PutMapping("agents/update")
    public ResponseEntity<Agent> updateAgent(@Valid @RequestBody AgentDto agentNewDetails) {
        Agent updatedAgent;
        try {
            updatedAgent = agentService.updateAgent(agentNewDetails);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedAgent);
    }

    @DeleteMapping("/deleteAgent/{agentId}")
    public ResponseEntity<Agent> deleteAgent(@PathVariable("agentId") String agentId) {
        Agent deletedAgent;
        try {
            deletedAgent = agentService.deleteAgentByAgentId(agentId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedAgent);
    }
}
