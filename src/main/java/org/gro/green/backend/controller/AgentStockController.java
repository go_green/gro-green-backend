package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.AgentStockItem;
import org.gro.green.backend.repository.model.OwnerStockItem;
import org.gro.green.backend.service.AgentService;
import org.gro.green.backend.service.AgentStockItemService;
import org.gro.green.backend.service.OwnerService;
import org.gro.green.backend.service.OwnerStockItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AgentStockController {

    @Autowired
    AgentStockItemService agentStockItemsService;

    @Autowired
    AgentService agentService;

    @GetMapping("/agentStockItem/all")
    public ResponseEntity<List<AgentStockItem>> getAllStockItems() {
        List<AgentStockItem> allStockItems;
        try {
            allStockItems = agentStockItemsService.getAllAgentStockItems();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allStockItems);
    }

    @GetMapping("/agentStockItem/{stockItem_id}")
    public ResponseEntity<AgentStockItem> getStockItemById(@PathVariable(value = "stockItem_id") int stockItemId) {
        AgentStockItem agentStockItem;
        try {
            agentStockItem = agentStockItemsService.getAgentStockItemById(String.valueOf(stockItemId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(agentStockItem);
    }

    @GetMapping("/agentStockItem/agent/{user_id}")
    public ResponseEntity<List<AgentStockItem>> getStockItemsByAgent(@PathVariable(value = "user_id") int userId) {
        List<AgentStockItem> agentStockItems;
        try {
            Agent agent = agentService.getAgentByUserId(String.valueOf(userId));
            agentStockItems = agentStockItemsService.getAgentStockItemsByAgentId(String.valueOf(agent.getAgentId()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(agentStockItems);
    }

    @PostMapping("/agentStockItem/register/{user_id}")
    public ResponseEntity<AgentStockItem> saveAgentStockItem(@PathVariable(value = "user_id") int userId, @Valid @RequestBody AgentStockItem agentStockItem){
        AgentStockItem updatedAgentStockItem;
        try {
            agentStockItem.setAgent(agentService.getAgentByUserId(String.valueOf(userId)));
            updatedAgentStockItem = agentStockItemsService.registerAgentStockItem(agentStockItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedAgentStockItem);

    }

    @PutMapping("/agentStockItem/updateStockItem/{user_id}/{stockItem_id}")
    public ResponseEntity<AgentStockItem> updateAgentStockItem(@PathVariable(value = "user_id") int userId,
                                                               @PathVariable(value = "stockItem_id") int stockItemId,
                                                               @Valid @RequestBody AgentStockItem agentStockItem){
        AgentStockItem updatedStockItem;
        try {
            agentStockItem.setAgent(agentService.getAgentByUserId(String.valueOf(userId)));
            updatedStockItem = agentStockItemsService.updateAgentStockItemById(String.valueOf(stockItemId), agentStockItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedStockItem);

    }

    @Transactional
    @DeleteMapping("/agentStockItem/deleteStockItem/{stockItemId}")
    public ResponseEntity<AgentStockItem> deleteProductItem(@PathVariable("stockItemId") String stockItemId) {
        AgentStockItem deletedStockItem;
        try {
            deletedStockItem = agentStockItemsService.deleteAgentStockItemById(stockItemId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedStockItem);
    }
}
