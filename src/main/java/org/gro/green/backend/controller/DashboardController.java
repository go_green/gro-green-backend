package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.dto.CountAdminDto;
import org.gro.green.backend.repository.model.dto.CountAgentDto;
import org.gro.green.backend.service.AgentService;
import org.gro.green.backend.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class DashboardController {

    @Autowired
    DashboardService dashboardService;

    @Autowired
    AgentService agentService;

    @GetMapping("/admin/count")
    public ResponseEntity<CountAdminDto> getCountsForAdmin() {
        CountAdminDto countAdmin;
        try {
            countAdmin = dashboardService.getCountForAdmin();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(countAdmin);
    }

    @GetMapping("/agent/count/{userId}")
    public ResponseEntity<CountAgentDto> getCountsForAgent(@PathVariable("userId") String userId) {
        CountAgentDto countAgentDto;
        Agent agent;
        try {
            agent = agentService.getAgentByUserId(userId);
            countAgentDto = dashboardService.getCountForAgent(agent);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(countAgentDto);
    }
}
