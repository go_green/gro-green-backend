package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.dto.DistributionRequestDto;
import org.gro.green.backend.repository.model.dto.DistributionResDto;
import org.gro.green.backend.repository.model.dto.DistributionStatusUpdateReqDto;
import org.gro.green.backend.repository.model.dto.DistributionListReqDto;
import org.gro.green.backend.service.DistributionService;
import org.gro.green.backend.service.impl.DistributionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DistributionController {

    @Autowired
    DistributionFactory distributionFactory;

    @PostMapping("/distribution/distribute/{stockType}")
    public ResponseEntity<DistributionResDto> createDistribution(@Valid @RequestBody DistributionRequestDto distributionRequestDto, @PathVariable("stockType") String stockType) {
        DistributionService distributionService = distributionFactory.getDistributionService(stockType);
        DistributionResDto ownerDistribution;
        try {
            ownerDistribution = distributionService.distributeItems(distributionRequestDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(ownerDistribution);
    }

    @PostMapping("/distribution/get/{stockType}")
    public ResponseEntity<List<DistributionResDto>> getDistribution(@Valid @RequestBody DistributionListReqDto distributionListReqDto, @PathVariable("stockType") String stockType) {
        DistributionService distributionService = distributionFactory.getDistributionService(stockType);
        List<DistributionResDto> pendingDistribution;
        try {
            pendingDistribution = distributionService.getPendingDistribution(distributionListReqDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(pendingDistribution);
    }

    @PostMapping("/distribution/status/update/{stockType}")
    public ResponseEntity<DistributionResDto> updateDistributionStatus(@Valid @RequestBody DistributionStatusUpdateReqDto distributionStatusUpdateReqDto, @PathVariable("stockType") String stockType) {
        DistributionService distributionService = distributionFactory.getDistributionService(stockType);
        DistributionResDto distributionResDto;
        try {
            distributionResDto = distributionService.updateStatus(distributionStatusUpdateReqDto);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(distributionResDto);
//        return null;
    }

    @GetMapping("/distribution/get/{stockType}/{id}/{receiverRole}")
    public ResponseEntity<List<DistributionResDto>> getAllDistribution(@PathVariable("stockType") String stockType,
                                                                       @PathVariable("id") int id,
                                                                       @PathVariable("receiverRole") String receiverRole) {
        DistributionService distributionService = distributionFactory.getDistributionService(stockType);
        List<DistributionResDto> distributionList;
        try {
            distributionList = distributionService.getDistributionList(id,receiverRole);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(distributionList);


    }
}
