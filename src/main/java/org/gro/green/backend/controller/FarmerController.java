package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.dto.FarmerDto;
import org.gro.green.backend.repository.model.dto.FarmerResDto;
import org.gro.green.backend.service.FarmerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class FarmerController {
    @Autowired
    FarmerService farmerService;

    @GetMapping("/farmers/all")
    public ResponseEntity<List<Farmer>> getAllFarmers() {
        List<Farmer> allAgents;
        try {
            allAgents = farmerService.getAllFarmers();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allAgents);
    }

    @GetMapping("/farmers/all_with_locations")
    public ResponseEntity<List<FarmerResDto>> getAllFarmersWithLocations() {
        List<FarmerResDto> allFarmers;
        try {
            allFarmers = farmerService.getAllFarmersWithLocations();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allFarmers);
    }

    @GetMapping("/farmers/filter_by_agent/{userId}")
    public ResponseEntity<List<FarmerResDto>> getFarmersByAgentId(@PathVariable("userId") String userId) {
        List<FarmerResDto> allFarmers;
        try {
            allFarmers = farmerService.getFarmersByAgent(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allFarmers);
    }

    @PostMapping("farmer/register")
    public ResponseEntity<Farmer> saveFarmer(@Valid @RequestBody FarmerDto farmerDto){
        Farmer updatedFarmer;
        try {
            updatedFarmer = farmerService.registerFarmer(farmerDto);
        } catch (Exception e) {
            System.out.println("failed"  + e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedFarmer);
    }

    @PutMapping("farmer/update")
    public ResponseEntity<Farmer> updateFarmer(@Valid @RequestBody FarmerDto farmerNewDetails){
        Farmer updatedFarmer;
        try {
            updatedFarmer = farmerService.updateFarmer(farmerNewDetails);
        } catch (Exception e) {
            System.out.println("failed"  + e.toString());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedFarmer);
    }

    @Transactional
    @DeleteMapping("/farmer/delete/{farmerId}")
    public ResponseEntity<Farmer> deleteProductItem(@PathVariable("farmerId") String farmerId) {
        Farmer deletedFarmer;
        try {
            deletedFarmer = farmerService.deleteFarmerById(farmerId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedFarmer);
    }
}
