package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Feedback;
import org.gro.green.backend.repository.model.dto.FeedbackResDto;
import org.gro.green.backend.service.FeedbackService;
import org.gro.green.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class FeedbackController {

    @Autowired
    FeedbackService feedbackService;

    @Autowired
    UserService userService;

    @GetMapping("/feedback/all")
    public ResponseEntity<List<FeedbackResDto>> getAllFeedback() {
        List<FeedbackResDto> allFeedback;
        try {
            allFeedback = feedbackService.getAllFeedback();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allFeedback);
    }

    @GetMapping("/feedback/{feedback_id}")
    public ResponseEntity<Feedback> getFeedbackById(@PathVariable(value = "feedback_id") int feedbackId) {
        Feedback feedback;
        try {
            feedback = feedbackService.getFeedbackById(String.valueOf(feedbackId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(feedback);
    }

    @PostMapping("/feedback/register/{user_id}")
    public ResponseEntity<Feedback> saveFeedback(@PathVariable(value = "user_id") int userId, @Valid @RequestBody Feedback feedback){
        Feedback updatedFeedback;
        try {
            feedback.setUser(userService.getUserById(String.valueOf(userId)));
            updatedFeedback = feedbackService.registerFeedback(feedback);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedFeedback);

    }

    @PutMapping("/feedback/updateFeedback/{user_id}/{feedback_id}")
    public ResponseEntity<Feedback> updateFeedback(@PathVariable(value = "user_id") int userId,
                                                               @PathVariable(value = "feedback_id") int feedbackId,
                                                               @Valid @RequestBody Feedback feedback){
        Feedback updatedFeedback;
        try {
            feedback.setUser(userService.getUserById(String.valueOf(userId)));
            updatedFeedback = feedbackService.updateFeedbackById(String.valueOf(feedbackId), feedback);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedFeedback);

    }

    @Transactional
    @DeleteMapping("/feedback/deleteFeedback/{feedbackId}")
    public ResponseEntity<Feedback> deleteProductItem(@PathVariable("feedbackId") String feedbackId) {
        Feedback deletedFeedback;
        try {
            deletedFeedback = feedbackService.deleteFeedbackById(feedbackId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedFeedback);
    }
}
