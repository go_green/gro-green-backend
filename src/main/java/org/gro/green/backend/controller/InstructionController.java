package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Instruction;
import org.gro.green.backend.repository.model.ProductItem;
import org.gro.green.backend.repository.model.dto.UploadFileResponse;
import org.gro.green.backend.service.InstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InstructionController {
    @Autowired
    InstructionService instructionService;

    @GetMapping("/instruction/all")
    public ResponseEntity<List<Instruction>> getAllInstructions() {
        List<Instruction> allInstructions;
        try {
            allInstructions = instructionService.getAllInstructions();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allInstructions);
    }

    @PostMapping("/instruction/register")
    public ResponseEntity<Instruction> saveInstruction(@Valid @RequestBody Instruction instruction){
        Instruction updatedInstruction;
        try {
            updatedInstruction = instructionService.registerInstruction(instruction);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedInstruction);

    }

    @PutMapping("/instruction/setProductItem/{instruction_id}")
    public ResponseEntity<Instruction> addProductItemToInstruction(@PathVariable(value = "instruction_id") int instructionId, @Valid @RequestBody ProductItem productItem){
        Instruction updatedInstruction;
        try {
            Instruction instruction = instructionService.getInstructionById(String.valueOf(instructionId));
            instruction.setProductItem(productItem);
            updatedInstruction = instructionService.updateInstructionById(String.valueOf(instructionId), instruction);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedInstruction);

    }
}
