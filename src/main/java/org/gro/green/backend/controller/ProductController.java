package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.ProductItem;
import org.gro.green.backend.service.ProductItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ProductController {
    @Autowired
    ProductItemService productItemService;

    @GetMapping("/productItem/all")
    public ResponseEntity<List<ProductItem>> getAllProductItems() {
        List<ProductItem> allProductItems;
        try {
            allProductItems = productItemService.getAllProductItems();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allProductItems);
    }

    @GetMapping("/productItem/{productItem_id}")
    public ResponseEntity<ProductItem> getProductItemById(@PathVariable(value = "productItem_id") int productItemId) {
        ProductItem productItem;
        try {
            productItem = productItemService.getProductItemById(String.valueOf(productItemId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(productItem);
    }

    @PostMapping("/productItem/register")
    public ResponseEntity<ProductItem> saveProductItem(@Valid @RequestBody ProductItem productItem){
        ProductItem updatedProductItem;
        try {
            updatedProductItem = productItemService.registerProductItem(productItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedProductItem);

    }

    @PutMapping("/productItem/updateProductItem/{productItem_id}")
    public ResponseEntity<ProductItem> updateProductItem(@PathVariable(value = "productItem_id") int productItemId, @Valid @RequestBody ProductItem productItem){
        ProductItem updatedProductItem;
        try {
            updatedProductItem = productItemService.updateProductItemById(String.valueOf(productItemId), productItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedProductItem);

    }

    @Transactional
    @DeleteMapping("/productItem/deleteProductItem/{productItemId}")
    public ResponseEntity<ProductItem> deleteProductItem(@PathVariable("productItemId") String productItemId) {
        ProductItem deletedProductItem;
        try {
            deletedProductItem = productItemService.deleteProductItemById(productItemId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedProductItem);
    }
}
