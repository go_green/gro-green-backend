package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.OwnerStockItem;
import org.gro.green.backend.service.OwnerService;
import org.gro.green.backend.service.OwnerStockItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class StockController {

    @Autowired
    OwnerStockItemsService ownerStockItemsService;

    @Autowired
    OwnerService ownerService;

    @GetMapping("/ownerStockItem/all")
    public ResponseEntity<List<OwnerStockItem>> getAllStockItems() {
        List<OwnerStockItem> allStockItems;
        try {
            allStockItems = ownerStockItemsService.getAllOwnerStockItems();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allStockItems);
    }

    @GetMapping("/ownerStockItem/{stockItem_id}")
    public ResponseEntity<OwnerStockItem> getStockItemById(@PathVariable(value = "stockItem_id") int stockItemId) {
        OwnerStockItem ownerStockItem;
        try {
            ownerStockItem = ownerStockItemsService.getOwnerStockItemsById(String.valueOf(stockItemId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(ownerStockItem);
    }

    @PostMapping("/ownerStockItem/register/{user_id}")
    public ResponseEntity<OwnerStockItem> saveOwnerStockItem(@PathVariable(value = "user_id") int userId, @Valid @RequestBody OwnerStockItem ownerStockItem){
        OwnerStockItem updatedOwnerStockItem;
        try {
            ownerStockItem.setOwner(ownerService.getOwnerByUserId(String.valueOf(userId)));
            updatedOwnerStockItem = ownerStockItemsService.registerOwnerStockItems(ownerStockItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedOwnerStockItem);

    }

    @PutMapping("/ownerStockItem/updateStockItem/{user_id}/{stockItem_id}")
    public ResponseEntity<OwnerStockItem> updateOwnerStockItem(@PathVariable(value = "user_id") int userId,
                                                               @PathVariable(value = "stockItem_id") int stockItemId,
                                                               @Valid @RequestBody OwnerStockItem ownerStockItem){
        OwnerStockItem updatedStockItem;
        try {
            ownerStockItem.setOwner(ownerService.getOwnerByUserId(String.valueOf(userId)));
            updatedStockItem = ownerStockItemsService.updateOwnerStockItemsById(String.valueOf(stockItemId), ownerStockItem);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedStockItem);

    }

    @Transactional
    @DeleteMapping("/ownerStockItem/deleteStockItem/{stockItemId}")
    public ResponseEntity<OwnerStockItem> deleteOwnerStockItem(@PathVariable("stockItemId") String stockItemId) {
        OwnerStockItem deletedStockItem;
        try {
            deletedStockItem = ownerStockItemsService.deleteOwnerStockItemsById(stockItemId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedStockItem);
    }
}
