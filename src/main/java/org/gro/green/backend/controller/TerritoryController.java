package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.Territory;
import org.gro.green.backend.service.TerritoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class TerritoryController {

    @Autowired
    private TerritoryService territoryService;

    @GetMapping("/territory-management/territories")
    public ResponseEntity<List<Territory>> getAllTerritories() {
        List<Territory> territory = null;
//        Response<Territory> territoryResponse = new Response<>();
        try {
            territory = territoryService.getAllTerritorys();
//            territoryResponse.setStatus(HttpStatus.OK);
//            territoryResponse.setMessage("Success");
//            territoryResponse.setData(territory);
        } catch (Exception e) {
//            territoryResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//            territoryResponse.setMessage("Error");
//            territoryResponse.setData(territory);
//
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(territory);
    }

    @GetMapping("/territory-management/territories/agent/{agent_id}")
    public ResponseEntity<List<Territory>> getTerritoriesByAgentId(@PathVariable(value = "agent_id") int agentId) {
        List<Territory> territories;
        try {
            territories = territoryService.getTerritoriesByAgentId(agentId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok(territories);
    }
}
