package org.gro.green.backend.controller;

import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.ChangePasswordRequestDto;
import org.gro.green.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping("/users/all")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> allUsers;
        try {
            allUsers = userService.getAllUsers();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(allUsers);
    }

    @GetMapping("/users/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable("userId") String userId) {
        User user;
        try {
            user = userService.getUserById(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(user);
    }


    @PostMapping("/users/register")
    public ResponseEntity<User> addUser(@Valid @RequestBody User user) {
        User newUser;
        try {
            newUser = userService.registerUser(user);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(newUser);
    }

    @PutMapping("/users/updatePassword/{userId}")
    public ResponseEntity<User> updatePasswordUser(@PathVariable(value = "userId") int userId, @Valid @RequestBody ChangePasswordRequestDto changePasswordRequest) {
        User updatedUser;
        try {
            authenticateForUpdatePassword(changePasswordRequest.getUsername(), changePasswordRequest.getCurrentPassword());
            updatedUser = userService.updateUserPasswordById(userId, changePasswordRequest);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    @PutMapping("/users/resetPassword/{userId}")
    public ResponseEntity<User> resetUserPassword(@PathVariable(value = "userId") int userId, @Valid @RequestBody ChangePasswordRequestDto changePasswordRequest) {
        User updatedUser;
        try {
//            authenticateForUpdatePassword(changePasswordRequest.getUsername(), changePasswordRequest.getCurrentPassword());
            updatedUser = userService.updateUserPasswordById(userId, changePasswordRequest);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    @PutMapping("/updateUser/{userId}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "userId") String userId, @Valid @RequestBody User userNewDetails) {
        User updatedUser;
        try {
            updatedUser = userService.updateUserById(userId, userNewDetails);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    @PutMapping("/updateUserStatus")
    public ResponseEntity<User> updateUserStatus(@Valid @RequestBody User userNewDetails) {
        User updatedUser;
        try {
            updatedUser = userService.updateUserStatusById(userNewDetails);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(updatedUser);
    }

    @Transactional
    @DeleteMapping("/deleteUser/{userId}")
    public ResponseEntity<User> deleteUser(@PathVariable("userId") String userId) {
        User deletedUser;
        try {
            deletedUser = userService.deleteUserById(userId);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        return ResponseEntity.ok(deletedUser);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    private void authenticateForUpdatePassword(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
//            e.printStackTrace();
            throw new UsernameNotFoundException("User not found with username: " + username);

        } catch (BadCredentialsException e) {
//            e.printStackTrace();
            throw new UsernameNotFoundException("User bad credential with username: " + username);
        }
    }
}
