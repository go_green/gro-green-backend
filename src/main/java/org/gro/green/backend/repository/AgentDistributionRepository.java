package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.AgentDistribution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AgentDistributionRepository extends JpaRepository<AgentDistribution, Long> {
    AgentDistribution findAgentDistributionById(int id);
    AgentDistribution deleteAgentDistributionById(int id);

    @Query(value = "SELECT COUNT(agent_distribution_id) AS NumberOfPendingDist FROM grogreen.agent_distribution where status = 'pending'", nativeQuery = true)
    int getPendingDistributionCount();
}