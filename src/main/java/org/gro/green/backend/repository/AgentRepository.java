package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {
    Agent findAgentByAgentId(int agentId);
    Agent deleteAgentByAgentId(int agentId);
    Agent findAgentByUserId(int id);
    List<Agent> findAgentsByUser_Status(String status);
    @Query(value = "SELECT COUNT(agent_id) AS NumberOfAgents FROM grogreen.agent", nativeQuery = true)
    int getAgentCount();
}
