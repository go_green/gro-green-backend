package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.AgentStockItemDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgentStockItemDistributionRepository extends JpaRepository<AgentStockItemDistribution, Long> {
    AgentStockItemDistribution findAgentDistributionById(int id);
    AgentStockItemDistribution deleteAgentDistributionById(int id);
    List<AgentStockItemDistribution> findAgentStockItemDistributionsByAgentDistribution_Id(int distributionId);
    List<AgentStockItemDistribution> findAgentStockItemDistributionsByAgentStockItem_Agent(Agent agent);
    int countAgentStockItemDistributionsByAgentDistribution_StatusAndAgentStockItem_Agent(String status, Agent agent);
}
