package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.AgentStockItemOwnerDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AgentStockItemOwnerDistributionRepository extends JpaRepository<AgentStockItemOwnerDistribution, Long> {
    AgentStockItemOwnerDistribution findStockItemOwnerDistributionById(int id);
    AgentStockItemOwnerDistribution deleteStockItemOwnerDistributionById(int id);
    List<AgentStockItemOwnerDistribution> findAgentStockItemOwnerDistributionsByAgentStockItem_AgentAndOwnerDistributionStatus(
            Agent agent,
            String status
    );
    List<AgentStockItemOwnerDistribution> findAgentStockItemOwnerDistributionsByOwnerDistributionId(int distributionId);
}
