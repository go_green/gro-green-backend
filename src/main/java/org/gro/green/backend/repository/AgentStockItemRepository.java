package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.AgentStockItem;
import org.gro.green.backend.repository.model.ProductItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public interface AgentStockItemRepository extends JpaRepository<AgentStockItem, Integer> {
    AgentStockItem findAgentStockItemById(int id);

    AgentStockItem deleteAgentStockItemById(int id);

    @Query(value = "SELECT * FROM grogreen.agent_stock_item where agent_id = ?1", nativeQuery = true)
    List<AgentStockItem> findAgentStockItemsByAgentId(int id);

    List<AgentStockItem> findAgentStockItemByAgentAndProductItem(Agent agent, ProductItem productItem);

    @Query("SELECT a FROM AgentStockItem a WHERE a.agent = ?1 AND a.productItem.id = ?2")
    AgentStockItem findAgentStockItemByAgentAndProductItem2(int agentId, int productItemId);

    List<AgentStockItem> findAllByIdIn(ArrayList<Integer> productItems);
}