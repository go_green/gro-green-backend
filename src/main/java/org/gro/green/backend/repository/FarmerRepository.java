package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.Farmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FarmerRepository extends JpaRepository<Farmer, Long> {
    Farmer findFarmerByFarmerId(int id);
    Farmer findFarmerByUser_Id(int id);
    Farmer deleteFarmerByFarmerId(int id);

    @Query(value = "SELECT * FROM grogreen.farmer where agent_id = ?1", nativeQuery = true)
    List<Farmer> findAllByAgentId(int agentId);

    @Query(value = "SELECT COUNT(farmer_id) AS NumberOfFarmers FROM grogreen.farmer", nativeQuery = true)
    int getFarmerCount();

    int countFarmersByAgent(Agent agent);
}