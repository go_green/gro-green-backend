package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.FarmerStockItemAgentDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FarmerStockItemAgentDistributionRepository extends JpaRepository<FarmerStockItemAgentDistribution, Long> {
    FarmerStockItemAgentDistribution findFarmerStockItemAgentDistributionById(int id);
    FarmerStockItemAgentDistribution deleteFarmerStockItemAgentDistributionById(int id);
    List<FarmerStockItemAgentDistribution>
        findFarmerStockItemAgentDistributionsByFarmerStockItem_FarmerAndAgentDistribution_Status(Farmer farmer,
                                                                                             String status);
    List<FarmerStockItemAgentDistribution>
        findFarmerStockItemAgentDistributionsByAgentDistribution_Id(int distributionId);
}
