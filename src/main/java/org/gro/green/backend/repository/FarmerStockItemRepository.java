package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.FarmerStockItem;
import org.gro.green.backend.repository.model.ProductItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FarmerStockItemRepository extends JpaRepository<FarmerStockItem, Long> {
    FarmerStockItem findFarmerStockItemById(int id);
    FarmerStockItem deleteFarmerStockItemById(int id);
    List<FarmerStockItem> findFarmerStockItemByFarmerAndProductItem(Farmer farmer, ProductItem productItem);
//    List<FarmerStockItem> findFarmerStockItemByFarmerAndProductItem
}
