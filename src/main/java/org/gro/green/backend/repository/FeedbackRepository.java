package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Feedback;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FeedbackRepository extends JpaRepository<Feedback, Long> {
    Feedback findFeedbackByFeedbackId(int id);
    Feedback deleteFeedbackByFeedbackId(int id);
    Feedback findFeedbackByUserId(int id);

}
