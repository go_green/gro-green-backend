package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Long> {
    Image findImageById(int id);
    Image deleteImageById(int id);
}
