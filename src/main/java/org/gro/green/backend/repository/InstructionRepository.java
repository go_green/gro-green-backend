package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Instruction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstructionRepository extends JpaRepository<Instruction, Long> {
    Instruction findInstructionById(int id);
    Instruction deleteInstructionById(int id);
}