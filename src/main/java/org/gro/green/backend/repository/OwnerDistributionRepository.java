package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.OwnerDistribution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OwnerDistributionRepository extends JpaRepository<OwnerDistribution, Integer> {
    OwnerDistribution findOwnerDistributionById(int id);
//    List<OwnerDistribution> findAllByRec
    OwnerDistribution deleteOwnerDistributionById(int id);
    List<OwnerDistribution> findOwnerDistributionsByReceiverRole(String receiverRole);
//    new org.gro.green.backend.repository.model.
//    @Query("SELECT new org.gro.green.backend.repository.model.dto.DistributionResDto(o.id,o.sendDate,o.receivedDate,o.detail,o.status) FROM OwnerDistribution o left join org.gro.green.backend.repository.model.AgentStockItemOwnerDistribution a on o.id = a.ownerDistribution.id where a.agentStockItem.agent = ?1")
//    List<OwnerDistribution> findOwnerDistributionsByAgentIdAndStatus(Agent agent);
    @Query(value = "SELECT COUNT(owner_distribution_id) AS NumberOfPendingDist FROM grogreen.owner_distribution where status = 'pending'", nativeQuery = true)
    int getPendingDistributionCount();

    @Query(value = "SELECT COUNT(owner_distribution_id) AS NumberOfRejectedDist FROM grogreen.owner_distribution where status = 'rejected'", nativeQuery = true)
    int getRejectedDistributionCount();

    @Query(value = "SELECT COUNT(owner_distribution_id) AS NumberOfReceivedDist FROM grogreen.owner_distribution where status = 'received'", nativeQuery = true)
    int getReceivedDistributionCount();
}
