package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Owner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OwnerRepository extends JpaRepository<Owner, Long> {
    Owner findOwnerByOwnerId(int id);
    Owner deleteOwnerByOwnerId(int id);
    Owner findOwnerByUserId(int id);
}
