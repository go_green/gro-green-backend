package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.OwnerStockItemDistribution;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OwnerStockItemDistributionRepository extends JpaRepository<OwnerStockItemDistribution, Long> {
    OwnerStockItemDistribution findOwnerStockItemDistributionById(int id);
    OwnerStockItemDistribution deleteOwnerStockItemDistributionById(int id);
    List<OwnerStockItemDistribution> findOwnerStockItemDistributionsByOwnerDistributionIdAndOwnerDistributionReceiverRole(int distributionId, String receiverRole);
}
