package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.OwnerStockItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OwnerStockItemsRepository extends JpaRepository<OwnerStockItem, Integer> {
    OwnerStockItem findOwnerStockItemsById(int id);
    OwnerStockItem deleteOwnerStockItemsById(int id);
    List<OwnerStockItem> findAllByIdIn(List<Integer> productItems);
}