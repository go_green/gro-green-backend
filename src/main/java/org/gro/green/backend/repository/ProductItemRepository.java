package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.ProductItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductItemRepository extends JpaRepository<ProductItem, Long> {
    ProductItem findProductItemById(int id);
    ProductItem deleteProductItemById(int id);

    @Query(value = "SELECT COUNT(id) AS NumberOfProducts FROM grogreen.product_item", nativeQuery = true)
    int getProductItemCount();
}