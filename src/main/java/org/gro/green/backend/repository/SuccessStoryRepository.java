package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.SuccessStory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SuccessStoryRepository extends JpaRepository<SuccessStory, Long> {
    SuccessStory findSuccessStoriesById(int id);
    SuccessStory deleteSuccessStoriesById(int id);
}