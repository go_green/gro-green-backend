package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.Territory;
import org.gro.green.backend.repository.model.dto.LocationDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.List;

public interface TerritoryRepository extends JpaRepository<Territory, Long> {
    Territory findTerritoriesByLocationId(int id);
    Territory deleteTerritoriesByLocationId(int id);
    @Query(value = "SELECT * FROM grogreen.territory inner join grogreen.farmer using(farmer_id) inner join grogreen.agent using (agent_id) where grogreen.agent.agent_id = ?1", nativeQuery = true)
    List<Territory> findByAgentId(int agentId);

    @Query("SELECT new org.gro.green.backend.repository.model.dto.LocationDto(t.lat,t.lng) FROM Territory t WHERE t.farmer.farmerId = ?1")
    List<LocationDto> findTerritoriesByFarmerFarmerId(int farmerId);
}
