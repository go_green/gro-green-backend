package org.gro.green.backend.repository;

import org.gro.green.backend.repository.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findUserById(int id);
    User findUserByUsername(String username);
    User deleteUserById(int id);
}

