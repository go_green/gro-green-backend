package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "agent_distribution")
@AllArgsConstructor
@NoArgsConstructor
public class AgentDistribution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "agent_distribution_id")
    private int id;

    @Column(name = "send_date")
    @NotNull
    private LocalDateTime sendDate;

    @Column(name = "received_date")
    @Nullable
    private LocalDateTime receivedDate;

    @Column(name = "detail")
    private String detail;

    @Column(name = "status")
    @NotNull
    private String status;
}
