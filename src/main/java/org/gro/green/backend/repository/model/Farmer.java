package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "farmer")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Farmer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "farmer_id")
    private int farmerId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "agent_id")
    private Agent agent;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "imageUrl")
    private String imageUrl;

    @Column(name = "items_grow")
    private String itemsGrow;

    @Column(name = "cultivation_area")
    private Double cultivationArea;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "address")
    private String address;
}
