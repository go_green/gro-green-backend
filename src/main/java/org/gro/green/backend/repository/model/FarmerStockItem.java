package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "farmer_stock_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FarmerStockItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "farmer_stock_item_id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_item_id")
    @NotNull
    private ProductItem productItem;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "farmer_id")
    @NotNull
    private Farmer farmer;

    @Column(name = "qty")
    private double qty;
}
