package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "farmer_stock_item_distribution")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FarmerStockItemAgentDistribution implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "farmer_stock_item_id")
    @NotNull
    private FarmerStockItem farmerStockItem;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "agent_distribution_id")
    @NotNull
    private AgentDistribution agentDistribution;

    @Column(name = "qty")
    private double qty;

    @Column(name = "earned_points")
    private double earnedPoints;
}
