package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Table(name = "owner_distribution")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OwnerDistribution implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "owner_distribution_id")
    private int id;

    @Column(name = "send_date")
    @NotNull
    private LocalDateTime sendDate;

    @Nullable
    @Column(name = "received_date")
    private LocalDateTime receivedDate;

    @Column(name = "detail")
    private String detail;

    @Column(name = "receiver_role")
    private String receiverRole;

    @Column(name = "status")
    @NotNull
    private String status;

}
