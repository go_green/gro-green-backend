package org.gro.green.backend.repository.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "owner_stock_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OwnerStockItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "owner_stock_item_id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_item_id")
    @NotNull
    @Nullable
    private ProductItem productItem;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_id")
    @NotNull
    @Nullable
    private Owner owner;

    @Column(name = "quantity")
    @Nullable
    private double quantity;
}

