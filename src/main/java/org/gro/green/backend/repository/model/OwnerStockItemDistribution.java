package org.gro.green.backend.repository.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "owner_stock_item_distribution")
public class OwnerStockItemDistribution implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_stock_item_id")
    @NotNull
    private OwnerStockItem ownerStockItem;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_distribution_id")
    @NotNull
    private OwnerDistribution ownerDistribution;

    @Column(name = "qty")
    private double qty;
}
