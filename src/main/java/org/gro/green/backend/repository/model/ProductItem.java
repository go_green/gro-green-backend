package org.gro.green.backend.repository.model;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "product_item")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "product_name")
    @NotNull
    private String productName;

    @Lob
    @Column(name="description", length=512)
    @NotNull
    private String description;

    @Column(name = "points")
    @Nullable
    private double points;

    @Column(name = "type")
    @NotNull
    private String type;

    @Column(name = "imageUrl")
    @Nullable
    private String imageUrl;

    @OneToMany(mappedBy = "productItem", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Instruction> instructions;
}
