package org.gro.green.backend.repository.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
public class Response <T>{
    private HttpStatus status;
    private String message;
    private List<T> data;
}
