package org.gro.green.backend.repository.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "territory")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Territory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "location_id")
    private int locationId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "farmer_id")
    @NotNull
    private Farmer farmer;

    @Column(name = "lat")
    @NotNull
    private double lat;

    @Column(name = "lng")
    @NotNull
    private double lng;
}
