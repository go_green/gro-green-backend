package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class AgentDto {
    private int agentId;
    private int ownerId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String area;
    private String imageUrl;
    private String status;
    private String mobile;
    private String address;
}
