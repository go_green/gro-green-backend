package org.gro.green.backend.repository.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class CountAdminDto implements Serializable {

    private int productCount;
    private int farmerCount;
    private int agentCount;
    private DistributionCountDto distributionCount;
}
