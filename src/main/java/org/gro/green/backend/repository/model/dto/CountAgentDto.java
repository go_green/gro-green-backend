package org.gro.green.backend.repository.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CountAgentDto {
    private int farmerCount;
    private DistributionCountDto distributionCount;
}
