package org.gro.green.backend.repository.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public class DistributionCountDto implements Serializable {
    private int pendingCount;
    private int receivedCount;
    private int rejectedCount;
}
