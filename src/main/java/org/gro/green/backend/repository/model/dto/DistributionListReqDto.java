package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class DistributionListReqDto {
    int receiverId;
    String status;
    String receiverRole;
}
