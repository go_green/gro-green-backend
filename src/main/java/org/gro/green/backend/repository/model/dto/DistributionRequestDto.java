package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class DistributionRequestDto {
    int senderId;
    int receiverId;
    String status;
    String details;
    String receiverRole;
    List<ItemDto> items;
}
