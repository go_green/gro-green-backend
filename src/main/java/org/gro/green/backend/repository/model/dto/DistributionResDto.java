package org.gro.green.backend.repository.model.dto;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DistributionResDto<T> {
    private int id;
    private LocalDateTime sendDate;
    @Nullable
    private LocalDateTime receivedDate;
    private String detail;
    private String status;
    private List<T> items;
    private Object receiver;
}
