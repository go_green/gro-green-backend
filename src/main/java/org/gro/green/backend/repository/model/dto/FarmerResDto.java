package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.gro.green.backend.repository.model.Agent;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class FarmerResDto {
        private int userId;
        private int farmerId;
        private Agent agent;
        private String username;
        private String password;
        private String firstName;
        private String lastName;
        private String imageUrl;
        private String status;
        private List<LocationDto> territories;
        private String itemsGrow;
        private Double cultivationArea;
        private String mobile;
        private String address;

}
