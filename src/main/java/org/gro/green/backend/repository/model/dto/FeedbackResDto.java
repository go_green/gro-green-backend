package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackResDto {
    private int feedbackId;
    private String message;
    private User user;
    private Farmer farmer;
}
