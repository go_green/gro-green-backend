package org.gro.green.backend.repository.model.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}
