package org.gro.green.backend.repository.model.jwt;

import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.UserResponseDto;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    private final String jwttoken;
    private UserResponseDto user;


    public JwtResponse(String jwttoken, UserResponseDto user) {
        this.jwttoken = jwttoken;
        this.user = user;
    }

    public String getToken() {
        return this.jwttoken;
    }

    public UserResponseDto getUser() {
        return this.user;
    }
}
