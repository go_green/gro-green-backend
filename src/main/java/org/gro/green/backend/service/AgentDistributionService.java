package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.AgentDistribution;

import java.util.List;

public interface AgentDistributionService {
    AgentDistribution registerAgentDistribution(AgentDistribution agentDistribution);

    List<AgentDistribution> getAllAgentDistributions();

    AgentDistribution getAgentDistributionById(String Id);

    AgentDistribution updateAgentDistributionById(String Id, AgentDistribution agentDistribution);

    AgentDistribution deleteAgentDistributionById(String id);
}