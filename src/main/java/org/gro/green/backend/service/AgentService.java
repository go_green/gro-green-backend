package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.dto.AgentDto;

import java.util.List;

public interface AgentService {
    Agent registerAgent(AgentDto agentDto);

    List<Agent> getAllAgents();

    Agent getAgentByAgentId(String Id);

    Agent getAgentByUserId(String Id);

    Agent updateAgent(AgentDto agent);

    Agent deleteAgentByAgentId(String id);

    Agent saveAgent(Agent agent);

    List<Agent> getAllAgentsByStatus(String status);
}
