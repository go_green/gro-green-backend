package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.AgentStockItemDistribution;

import java.util.List;

public interface AgentStockItemDistributionService {
    AgentStockItemDistribution registerAgentStockItemDistribution(AgentStockItemDistribution agentStockItemDistribution);

    List<AgentStockItemDistribution> getAllAgentStockItemDistributions();

    AgentStockItemDistribution getAgentStockItemDistributionById(String Id);

    AgentStockItemDistribution updateAgentStockItemDistributionById(String Id, AgentStockItemDistribution agentStockItemDistribution);

    AgentStockItemDistribution deleteAgentStockItemDistributionById(String id);
}