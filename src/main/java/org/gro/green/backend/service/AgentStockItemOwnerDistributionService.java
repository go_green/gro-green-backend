package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.AgentStockItemOwnerDistribution;

import java.util.List;

public interface AgentStockItemOwnerDistributionService {
    AgentStockItemOwnerDistribution registerAgentStockItemOwnerDistribution(AgentStockItemOwnerDistribution agentStockItemOwnerDistribution);

    List<AgentStockItemOwnerDistribution> getAllAgentStockItemOwnerDistributions();

    AgentStockItemOwnerDistribution getAgentStockItemOwnerDistributionById(String Id);

    AgentStockItemOwnerDistribution updateAgentStockItemOwnerDistributionById(String Id, AgentStockItemOwnerDistribution agentStockItemOwnerDistribution);

    AgentStockItemOwnerDistribution deleteAgentStockItemOwnerDistributionById(String id);
}