package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.AgentStockItem;

import java.util.List;

public interface AgentStockItemService {
    AgentStockItem registerAgentStockItem(AgentStockItem agentStockItem);

    List<AgentStockItem> getAllAgentStockItems();

    AgentStockItem getAgentStockItemById(String Id);

    List<AgentStockItem> getAgentStockItemsByAgentId(String Id);

    AgentStockItem updateAgentStockItemById(String Id, AgentStockItem agentStockItem);

    AgentStockItem deleteAgentStockItemById(String id);
}