package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.dto.CountAdminDto;
import org.gro.green.backend.repository.model.dto.CountAgentDto;

public interface DashboardService {
    CountAdminDto getCountForAdmin();
    CountAgentDto getCountForAgent(Agent agent);
}
