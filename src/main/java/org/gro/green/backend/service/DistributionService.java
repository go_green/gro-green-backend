package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.dto.DistributionRequestDto;
import org.gro.green.backend.repository.model.dto.DistributionResDto;
import org.gro.green.backend.repository.model.dto.DistributionStatusUpdateReqDto;
import org.gro.green.backend.repository.model.dto.DistributionListReqDto;

import java.util.List;

public interface DistributionService {
    String getType();
    DistributionResDto distributeItems(DistributionRequestDto distributionRequestDto);
    Boolean checkSenderAvailability(int id);
    List<DistributionResDto> getPendingDistribution(DistributionListReqDto distributionListReqDto);
    DistributionResDto updateStatus(DistributionStatusUpdateReqDto distributionStatusUpdateReqDto);
    List<DistributionResDto> getDistributionList(int id, String receiverRole);
}
