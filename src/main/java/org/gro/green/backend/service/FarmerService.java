package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.dto.FarmerDto;
import org.gro.green.backend.repository.model.dto.FarmerResDto;

import java.util.List;

public interface FarmerService {
    Farmer saveFarmer(Farmer farmer);

    List<Farmer> getAllFarmers();

    Farmer getFarmerById(String Id);

    Farmer updateFarmer(FarmerDto farmer);

    Farmer deleteFarmerById(String id);

    Farmer registerFarmer(FarmerDto farmerDto);

    List<FarmerResDto> getAllFarmersWithLocations();

    List<FarmerResDto> getFarmersByAgent(String userId);
}