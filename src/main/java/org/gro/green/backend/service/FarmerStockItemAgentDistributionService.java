package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.FarmerStockItemAgentDistribution;

import java.util.List;

public interface FarmerStockItemAgentDistributionService {
    FarmerStockItemAgentDistribution registerFarmerStockItemAgentDistribution(FarmerStockItemAgentDistribution farmerStockItemAgentDistribution);

    List<FarmerStockItemAgentDistribution> getAllFarmerStockItemAgentDistributions();

    FarmerStockItemAgentDistribution getFarmerStockItemAgentDistributionById(String Id);

    FarmerStockItemAgentDistribution updateFarmerStockItemAgentDistributionById(String Id, FarmerStockItemAgentDistribution farmerStockItemAgentDistribution);

    FarmerStockItemAgentDistribution deleteFarmerStockItemAgentDistributionById(String id);
}