package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.FarmerStockItem;

import java.util.List;

public interface FarmerStockItemService {
    FarmerStockItem registerFarmerStockItem(FarmerStockItem farmerStockItem);

    List<FarmerStockItem> getAllFarmerStockItems();

    FarmerStockItem getFarmerStockItemById(String Id);

    FarmerStockItem updateFarmerStockItemById(String Id, FarmerStockItem farmerStockItem);

    FarmerStockItem deleteFarmerStockItemById(String id);
}