package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Feedback;
import org.gro.green.backend.repository.model.dto.FeedbackResDto;

import java.util.List;

public interface FeedbackService {
    Feedback registerFeedback(Feedback Owner);

    List<FeedbackResDto> getAllFeedback();

    Feedback getFeedbackById(String Id);

    Feedback getFeedbackByUserId(String userId);

    Feedback updateFeedbackById(String Id, Feedback Owner);

    Feedback deleteFeedbackById(String id);
}
