package org.gro.green.backend.service;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.Resource;

public interface FileStorageService {

//    public void FileStorageService(FileStorageProperties fileStorageProperties);

    public String storeFile(MultipartFile file);
    public Resource loadFileAsResource(String fileName);



}
