package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Image;

import java.util.List;

public interface ImageService {
    Image registerImage(Image image);

    List<Image> getAllImages();

    Image getImageById(String Id);

    Image updateImageById(String Id, Image image);

    Image deleteImageById(String id);
}