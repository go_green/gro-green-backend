package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Instruction;

import java.util.List;

public interface InstructionService {
    Instruction registerInstruction(Instruction instruction);

    List<Instruction> getAllInstructions();

    Instruction getInstructionById(String Id);

    Instruction updateInstructionById(String Id, Instruction instruction);

    Instruction deleteInstructionById(String id);
}