package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.OwnerDistribution;

import java.util.List;

public interface OwnerDistributionService {
    OwnerDistribution registerOwnerDistribution(OwnerDistribution ownerDistribution);

    List<OwnerDistribution> getAllOwnerDistributions();

    OwnerDistribution getOwnerDistributionById(String Id);

    OwnerDistribution updateOwnerDistributionById(String Id, OwnerDistribution ownerDistribution);

    OwnerDistribution deleteOwnerDistributionById(String id);
}