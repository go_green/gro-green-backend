package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Owner;

import java.util.List;

public interface OwnerService {
    Owner registerOwner(Owner Owner);

    List<Owner> getAllOwners();

    Owner getOwnerById(String Id);

    Owner getOwnerByUserId(String userId);

    Owner updateOwnerById(String Id, Owner Owner);

    Owner deleteOwnerById(String id);
}