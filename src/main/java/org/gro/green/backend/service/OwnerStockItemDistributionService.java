package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.OwnerStockItemDistribution;

import java.util.List;

public interface OwnerStockItemDistributionService {
    OwnerStockItemDistribution registerOwnerStockItemDistribution(OwnerStockItemDistribution ownerStockItemDistribution);

    List<OwnerStockItemDistribution> getAllOwnerStockItemDistributions();

    OwnerStockItemDistribution getOwnerStockItemDistributionById(String Id);

    OwnerStockItemDistribution updateOwnerStockItemDistributionById(String Id, OwnerStockItemDistribution ownerStockItemDistribution);

    OwnerStockItemDistribution deleteOwnerStockItemDistributionById(String id);
}