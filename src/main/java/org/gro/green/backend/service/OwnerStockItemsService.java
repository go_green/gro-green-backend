package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.OwnerStockItem;

import java.util.List;

public interface OwnerStockItemsService {
    OwnerStockItem registerOwnerStockItems(OwnerStockItem ownerStockItem);

    List<OwnerStockItem> getAllOwnerStockItems();

    OwnerStockItem getOwnerStockItemsById(String Id);

    OwnerStockItem updateOwnerStockItemsById(String Id, OwnerStockItem ownerStockItem);

    OwnerStockItem deleteOwnerStockItemsById(String id);
}