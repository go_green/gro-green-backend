package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.ProductItem;

import java.util.List;

public interface ProductItemService {
    ProductItem registerProductItem(ProductItem productItem);

    List<ProductItem> getAllProductItems();

    ProductItem getProductItemById(String Id);

    ProductItem updateProductItemById(String Id, ProductItem productItem);

    ProductItem deleteProductItemById(String id);
}