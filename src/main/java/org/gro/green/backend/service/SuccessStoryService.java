package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.SuccessStory;

import java.util.List;

public interface SuccessStoryService {
    SuccessStory registerSuccessStory(SuccessStory SuccessStory);

    List<SuccessStory> getAllSuccessStorys();

    SuccessStory getSuccessStoryById(String Id);

    SuccessStory updateSuccessStoryById(String Id, SuccessStory SuccessStory);

    SuccessStory deleteSuccessStoryById(String id);
}