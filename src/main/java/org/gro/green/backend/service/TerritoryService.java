package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.Territory;

import java.util.List;

public interface TerritoryService {
    Territory registerTerritory(Territory territory);

    List<Territory> getAllTerritorys();

    Territory getTerritoryById(String Id);

    Territory updateTerritoryById(String Id, Territory territory);

    Territory deleteTerritoryById(String id);

    List<Territory> getTerritoriesByAgentId(int agentId);
}