package org.gro.green.backend.service;

import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.ChangePasswordRequestDto;
import org.gro.green.backend.repository.model.dto.UserResponseDto;

import java.util.List;

public interface UserService {
    User registerUser(User user);

    List<User> getAllUsers();

    User getUserById(String Id);

    User getUserByUsername(String username);

    User updateUserById(String Id, User user);

    User deleteUserById(String id);

    User updateUserStatusById(User userNewDetails);

    UserResponseDto getUserWithUserIdByUsername(String username);

    User updateUserPasswordById(int userId, ChangePasswordRequestDto changePasswordRequest);
}
