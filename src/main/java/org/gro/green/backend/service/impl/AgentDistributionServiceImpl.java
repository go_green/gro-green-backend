package org.gro.green.backend.service.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.gro.green.backend.repository.*;
import org.gro.green.backend.repository.model.*;
import org.gro.green.backend.repository.model.dto.*;
import org.gro.green.backend.service.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Component
public class AgentDistributionServiceImpl implements DistributionService {
    @Autowired
    AgentRepository agentRepository;
    @Autowired
    FarmerRepository farmerRepository;
    @Autowired
    AgentStockItemRepository agentStockItemRepository;
    @Autowired
    AgentDistributionRepository agentDistributionRepository;
    @Autowired
    AgentStockItemDistributionRepository agentStockItemDistributionRepository;
    @Autowired
    FarmerStockItemRepository farmerStockItemRepository;
    @Autowired
    FarmerStockItemAgentDistributionRepository farmerStockItemAgentDistributionRepository;

    @Override
    public String getType() {
        return "agent";
    }

    @Override
    public DistributionResDto distributeItems(DistributionRequestDto distributionRequestDto) {
        if (checkSenderAvailability(distributionRequestDto.getSenderId())){
            ReceiverAvailability receiverAvailability = checkReceiverAvailability(distributionRequestDto.getReceiverId(), distributionRequestDto.getReceiverRole());
            if (receiverAvailability.getIsAvailable()) {
                Map<Integer, Double> requestedItemsMap = distributionRequestDto.getItems().stream().collect(Collectors.toMap(ItemDto::getStockItemId,ItemDto::getQuantity));
                AgentStockAvailability stockAvailability = checkStockAvailability(distributionRequestDto.getItems(),requestedItemsMap);
                if (stockAvailability.getIsAvailable()) {

                    AgentDistribution agentDistribution = saveAgentDistribution(distributionRequestDto);

                    List <AgentStockItem> availableStock = stockAvailability.getAgentStockItems();

                    List<AgentStockItem> updateAgentStockItems = updateAgentStock(availableStock,requestedItemsMap);

                    List<AgentStockItemDistribution> agentStockItemDistributions = saveAgentStockItemDistribution(updateAgentStockItems, requestedItemsMap, agentDistribution);

                    arrangeReceiverStock(receiverAvailability,updateAgentStockItems, agentDistribution,requestedItemsMap, distributionRequestDto.getReceiverRole());

                    return mapToDistributionResDto(agentDistribution, agentStockItemDistributions,receiverAvailability.getReceiver());

                } else {
                    throw new NoSuchElementException("Stock not available");
                }
            } else {
                throw new NoSuchElementException("Receiver not found");
            }
        }else {
            throw new NoSuchElementException("Sender not found");
        }
    }

    @Override
    public Boolean checkSenderAvailability(int id) {
        Agent agent = agentRepository.findAgentByAgentId(id);
        return agent != null;
    }

    @Override
    public List<DistributionResDto> getPendingDistribution(DistributionListReqDto distributionListReqDto) {
        ReceiverAvailability receiverAvailability = checkReceiverAvailability(distributionListReqDto.getReceiverId(), distributionListReqDto.getReceiverRole());
        if(receiverAvailability.getIsAvailable()) {
            Farmer farmer =(Farmer) receiverAvailability.getReceiver();
            try {
                List<FarmerStockItemAgentDistribution> farmerStockItems =
                        farmerStockItemAgentDistributionRepository
                                .findFarmerStockItemAgentDistributionsByFarmerStockItem_FarmerAndAgentDistribution_Status(
                                        farmer,
                                        distributionListReqDto.getStatus()
                                );
                Map<AgentDistribution, List<FarmerStockItemAgentDistribution>> farmerStockItemAgentDistributions = farmerStockItems.stream()
                        .collect(Collectors.groupingBy(FarmerStockItemAgentDistribution::getAgentDistribution));
                List<DistributionResDto> distributionResDtos = new ArrayList<>();
                for(Map.Entry<AgentDistribution, List<FarmerStockItemAgentDistribution>> farmerStockItemAgentDistribution:farmerStockItemAgentDistributions.entrySet() ){
                    distributionResDtos.add(
                            new DistributionResDto<FarmerStockItemAgentDistribution>(
                                    farmerStockItemAgentDistribution.getKey().getId(),
                                    farmerStockItemAgentDistribution.getKey().getSendDate(),
                                    farmerStockItemAgentDistribution.getKey().getReceivedDate(),
                                    farmerStockItemAgentDistribution.getKey().getStatus(),
                                    farmerStockItemAgentDistribution.getKey().getDetail(),
                                    farmerStockItemAgentDistribution.getValue(),
                                    farmer
                            )
                    );
                }

                return distributionResDtos;

            } catch (Exception e) {
                throw new NoSuchElementException("Persisting error");
            }

            }else {
            throw new NoSuchElementException("Receiver not found");
        }


//            return null;
    }

    @Override
    public DistributionResDto updateStatus(DistributionStatusUpdateReqDto distributionStatusUpdateReqDto) {
        if (distributionStatusUpdateReqDto.getStatus().equals("received")) {
            List<FarmerStockItemAgentDistribution> farmerStockItemAgentDistributions
                    = farmerStockItemAgentDistributionRepository
                    .findFarmerStockItemAgentDistributionsByAgentDistribution_Id(distributionStatusUpdateReqDto.getDistributionId());
            List<FarmerStockItem> updatedFarmerStockItem = updateFarmerStock(farmerStockItemAgentDistributions);
            AgentDistribution updatedAgentDistribution = updateAgentDistribution(distributionStatusUpdateReqDto);
            return mapToDistributionResDto(updatedAgentDistribution,updatedFarmerStockItem,null);

        } else if (distributionStatusUpdateReqDto.getStatus().equals("rejected")) {
            List<AgentStockItemDistribution> farmerStockItemAgentDistributions
                    = agentStockItemDistributionRepository
                    .findAgentStockItemDistributionsByAgentDistribution_Id(distributionStatusUpdateReqDto.getDistributionId());
            List<AgentStockItem> updatedFarmerStockItem = updateAgentStockWhenRejected(farmerStockItemAgentDistributions);
            AgentDistribution updatedAgentDistribution = updateAgentDistribution(distributionStatusUpdateReqDto);
            return mapToDistributionResDto(updatedAgentDistribution,updatedFarmerStockItem,null);

        }
            return null;
    }

    @Override
    public List<DistributionResDto> getDistributionList(int id, String receiverRole) {
        Agent agent = agentRepository.findAgentByAgentId(id);
        if (agent != null){
            List<AgentStockItemDistribution> agentStockItemDistributions = agentStockItemDistributionRepository.findAgentStockItemDistributionsByAgentStockItem_Agent(agent);
            List<AgentDistribution> agentDistributionsWithDuplicates= agentStockItemDistributions
                        .stream().map(agentStockItemDistribution -> agentStockItemDistribution.getAgentDistribution()).collect(Collectors.toList());
            List<AgentDistribution> agentDistributions = agentDistributionsWithDuplicates.stream().distinct().collect(Collectors.toList());
            List<DistributionResDto> distributionResDtosList = agentDistributions.stream().map(
                    agentDistribution -> {
                        Object receiver = new Object();
                        List<FarmerStockItemAgentDistribution> farmerStockItemAgentDistributions =
                                farmerStockItemAgentDistributionRepository
                                        .findFarmerStockItemAgentDistributionsByAgentDistribution_Id(agentDistribution.getId());
                        if (farmerStockItemAgentDistributions.size() > 0) {
                            Farmer receiveFarmer = farmerStockItemAgentDistributions.get(0).getFarmerStockItem().getFarmer();
                            receiver = receiveFarmer;
                        }
                        List<AgentStockItemDistribution> agentStockItemDistributionsList =
                                agentStockItemDistributionRepository
                                        .findAgentStockItemDistributionsByAgentDistribution_Id(agentDistribution.getId());
                        return mapToDistributionResDto(agentDistribution, agentStockItemDistributionsList, receiver);
                    }
            ).collect(Collectors.toList());
            return distributionResDtosList;
        }
        return null;
    }

    private List<AgentStockItem> updateAgentStockWhenRejected(List<AgentStockItemDistribution> farmerStockItemAgentDistributions) {
        List<AgentStockItem> updatedAgentStock = farmerStockItemAgentDistributions.stream()
                .map(farmerStockItemDistribution -> {
                            AgentStockItem agentStockItem = farmerStockItemDistribution.getAgentStockItem();
                            agentStockItem.setQuantity(agentStockItem.getQuantity() + farmerStockItemDistribution.getQty());
                            return agentStockItem;
                        }
                ).collect(Collectors.toList());
        return agentStockItemRepository.saveAll(updatedAgentStock);
    }

    private AgentDistribution updateAgentDistribution(DistributionStatusUpdateReqDto distribution) {
        AgentDistribution agentDistribution = agentDistributionRepository.findAgentDistributionById(distribution.getDistributionId());
        agentDistribution.setStatus(distribution.getStatus());
        agentDistribution.setReceivedDate(LocalDateTime.now());
        return agentDistributionRepository.save(agentDistribution);
    }

    private List<FarmerStockItem> updateFarmerStock(List<FarmerStockItemAgentDistribution> farmerStockItemAgentDistributions) {
        List<FarmerStockItem> updatedFarmerStock = farmerStockItemAgentDistributions.stream()
                .map(farmerStockItemAgentDistribution -> {
                            FarmerStockItem newFarmerStockItem = farmerStockItemAgentDistribution.getFarmerStockItem();
                            newFarmerStockItem.setQty(newFarmerStockItem.getQty() + farmerStockItemAgentDistribution.getQty());
                            return newFarmerStockItem;
                        }
                ).collect(Collectors.toList());
        return updatedFarmerStock;
    }

    private ReceiverAvailability checkReceiverAvailability(int receiverId, String receiverRole) {
        Farmer farmer = farmerRepository.findFarmerByFarmerId(receiverId);
        return new ReceiverAvailability(farmer != null, farmer);
    }

    private AgentStockAvailability checkStockAvailability(List<ItemDto> items, Map<Integer, Double> requestedItemsMap) {
        List<AgentStockItem> agentStockItems = agentStockItemRepository.findAllByIdIn(new ArrayList<>(requestedItemsMap.keySet()));
        List acceptedItemList = agentStockItems.stream().filter(agentStockItem ->
                agentStockItem.getQuantity() >= requestedItemsMap.get(agentStockItem.getId())
        ).collect(Collectors.toList());
        return new AgentStockAvailability(acceptedItemList.size() == items.size(), agentStockItems);
    }

    private AgentDistribution saveAgentDistribution(DistributionRequestDto distributionRequestDto) {
        AgentDistribution agentDistribution = new AgentDistribution(0,
                LocalDateTime.now(),
                null,
                distributionRequestDto.getDetails(),
                distributionRequestDto.getStatus());
        return agentDistributionRepository.save(agentDistribution);
    }

    private List<AgentStockItem> updateAgentStock(List<AgentStockItem> availableStock, Map<Integer, Double> requestedItemsMap) {
        List <AgentStockItem> updateStock = availableStock.stream().map(agentStockItem -> {
            AgentStockItem newStock = new AgentStockItem(
                    agentStockItem.getId(),
                    agentStockItem.getTitle(),
                    agentStockItem.getProductItem(),
                    agentStockItem.getAgent(),
                    agentStockItem.getQuantity() - requestedItemsMap.get(agentStockItem.getId())
            );
            return newStock;
        }).collect(Collectors.toList());
        return agentStockItemRepository.saveAll(updateStock);
    }

    private List<AgentStockItemDistribution> saveAgentStockItemDistribution(List<AgentStockItem> updateAgentStockItems, Map<Integer, Double> requestedItemsMap, AgentDistribution agentDistribution) {
        List <AgentStockItemDistribution> ownerStockItemDistributions =
                updateAgentStockItems.stream()
                        .map(updatedAgentStockItem -> {
                                    return new AgentStockItemDistribution(
                                            0,
                                            updatedAgentStockItem,
                                            agentDistribution,
                                            requestedItemsMap.get(updatedAgentStockItem.getId()));
                                }
                        ).collect(Collectors.toList());
        return agentStockItemDistributionRepository.saveAll(ownerStockItemDistributions);
    }

    private void arrangeReceiverStock(ReceiverAvailability receiverAvailability, List<AgentStockItem> updateAgentStockItems, AgentDistribution agentDistribution, Map<Integer, Double> requestedItemsMap, String receiverRole) {
        Farmer farmer =(Farmer) receiverAvailability.getReceiver();
        List<FarmerStockItemAgentDistribution> farmerStockItemAgentDistributions = new ArrayList<>();
        for (AgentStockItem agentStockItem:updateAgentStockItems) {
            ProductItem productItem = new ProductItem(agentStockItem.getProductItem().getId(),agentStockItem.getProductItem().getProductName(),
                    agentStockItem.getProductItem().getDescription(), agentStockItem.getProductItem().getPoints(), agentStockItem.getProductItem().getType(),
                    agentStockItem.getProductItem().getImageUrl(), agentStockItem.getProductItem().getInstructions());
            List<FarmerStockItem> farmerStockItem =
                    farmerStockItemRepository.findFarmerStockItemByFarmerAndProductItem(farmer, productItem);
            if (farmerStockItem.size() > 0) {
                FarmerStockItemAgentDistribution farmerStockItemAgentDistribution =
                        new FarmerStockItemAgentDistribution(
                                0,farmerStockItem.get(0),
                                agentDistribution,
                                requestedItemsMap.get(agentStockItem.getId()),
                                0
                        );
                farmerStockItemAgentDistributions.add(farmerStockItemAgentDistribution);

            } else {
                FarmerStockItem newFarmerStockItem = farmerStockItemRepository.save(
                        new FarmerStockItem(0, agentStockItem.getTitle(), agentStockItem.getProductItem(), farmer,0));
                FarmerStockItemAgentDistribution farmerStockItemAgentDistribution =
                        new FarmerStockItemAgentDistribution(
                                0,newFarmerStockItem,
                                agentDistribution,
                                requestedItemsMap.get(agentStockItem.getId()),
                                0
                        );
                farmerStockItemAgentDistributions.add(farmerStockItemAgentDistribution);

            }
        }
        farmerStockItemAgentDistributionRepository.saveAll(farmerStockItemAgentDistributions);

    }

    private DistributionResDto mapToDistributionResDto(AgentDistribution agentDistribution, List agentStockItemDistributions, Object receiver) {
        return new DistributionResDto(
                agentDistribution.getId(),
                agentDistribution.getSendDate(),
                agentDistribution.getReceivedDate(),
                agentDistribution.getDetail(),
                agentDistribution.getStatus(),
                agentStockItemDistributions,
                receiver
        );
    }
}

@Getter
@Setter
@AllArgsConstructor
class AgentStockAvailability{
    Boolean isAvailable;
    List<AgentStockItem> agentStockItems;
}

