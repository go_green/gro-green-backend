package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.AgentRepository;
import org.gro.green.backend.repository.OwnerRepository;
import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.Owner;
import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.AgentDto;
import org.gro.green.backend.service.AgentService;
import org.gro.green.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AgentServiceImpl implements AgentService {

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    UserService userService;

    @Override
    public Agent saveAgent(Agent agent) {
        return agentRepository.save(agent);
    }

    @Override
    public Agent registerAgent(AgentDto agentDto) {
        Agent newAgent;
        User user = new User(
                0,
                agentDto.getUsername(),
                agentDto.getPassword(),
                agentDto.getStatus(),
                "agent");
        User newUser = userService.registerUser(user);
        if(newUser != null) {
            Owner owner = ownerRepository.findOwnerByOwnerId(agentDto.getOwnerId());
            Agent agent = new Agent(0,
                    newUser,
                    owner,
                    agentDto.getFirstName(),
                    agentDto.getLastName(),
                    agentDto.getArea(),
                    agentDto.getImageUrl(),
                    agentDto.getMobile(),
                    agentDto.getAddress());
            newAgent = saveAgent(agent);
        } else {
            throw new NoSuchElementException("No such user create");
        }

        return newAgent;
    }

    @Override
    public List<Agent> getAllAgents() {
        return agentRepository.findAll();
    }

    @Override
    public List<Agent> getAllAgentsByStatus(String status) {
        return agentRepository.findAgentsByUser_Status(status);
    }

    @Override
    public Agent getAgentByAgentId(String agentId) {
        return agentRepository.findAgentByAgentId(Integer.parseInt(agentId));
    }

    @Override
    public Agent getAgentByUserId(String userId) {
        return agentRepository.findAgentByUserId(Integer.parseInt(userId));
    }

    @Override
    public Agent deleteAgentByAgentId(String agentId) {
        return agentRepository.deleteAgentByAgentId(Integer.parseInt(agentId));
    }

    @Override
    public Agent updateAgent(AgentDto agent) {
        Agent existingAgent = agentRepository.findAgentByAgentId(agent.getAgentId());
        Agent updatedAgent;
        if (existingAgent != null) {
            Owner owner = ownerRepository.findOwnerByOwnerId(agent.getOwnerId());
            User user = existingAgent.getUser();
            user.setStatus(agent.getStatus());
            existingAgent.setUser(user);
            existingAgent.setOwner(owner);
            existingAgent.setFirstName(agent.getFirstName());
            existingAgent.setLastName(agent.getLastName());
            existingAgent.setImageUrl(agent.getImageUrl());
            existingAgent.setArea(agent.getArea());
            existingAgent.setMobile(agent.getMobile());
            existingAgent.setAddress(agent.getAddress());
            updatedAgent = agentRepository.save(existingAgent);
        } else {
            throw new NoSuchElementException("Something went wrong");
        }
        return updatedAgent;
    }
}
