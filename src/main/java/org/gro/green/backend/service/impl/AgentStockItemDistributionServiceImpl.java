package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.AgentStockItemDistributionRepository;
import org.gro.green.backend.repository.model.AgentStockItemDistribution;
import org.gro.green.backend.service.AgentStockItemDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AgentStockItemDistributionServiceImpl implements AgentStockItemDistributionService {

    @Autowired
    AgentStockItemDistributionRepository agentStockItemDistributionRepository;

    @Override
    public AgentStockItemDistribution registerAgentStockItemDistribution(AgentStockItemDistribution agentStockItemDistribution) {
        return agentStockItemDistributionRepository.save(agentStockItemDistribution);
    }

    @Override
    public List<AgentStockItemDistribution> getAllAgentStockItemDistributions() {
        return agentStockItemDistributionRepository.findAll();
    }

    @Override
    public AgentStockItemDistribution getAgentStockItemDistributionById(String id) {
        return agentStockItemDistributionRepository.findAgentDistributionById(Integer.parseInt(id));
    }

    @Override
    public AgentStockItemDistribution updateAgentStockItemDistributionById(String id, AgentStockItemDistribution agentStockItemDistribution) {
        AgentStockItemDistribution existingAgentStockItemDistribution = agentStockItemDistributionRepository.findAgentDistributionById(Integer.parseInt(id));
        AgentStockItemDistribution updatedAgentStockItemDistribution;
        if (existingAgentStockItemDistribution != null) {
            agentStockItemDistribution.setId(existingAgentStockItemDistribution.getId());
            updatedAgentStockItemDistribution = agentStockItemDistributionRepository.save(agentStockItemDistribution);
        } else {
            throw new NoSuchElementException("No such agent stock item distribution found");
        }
        return updatedAgentStockItemDistribution;
    }

    @Override
    public AgentStockItemDistribution deleteAgentStockItemDistributionById(String id) {
        return agentStockItemDistributionRepository.deleteAgentDistributionById(Integer.parseInt(id));
    }
}
