package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.AgentStockItemOwnerDistributionRepository;
import org.gro.green.backend.repository.model.AgentStockItemOwnerDistribution;
import org.gro.green.backend.service.AgentStockItemOwnerDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AgentStockItemOwnerDistributionServiceImpl implements AgentStockItemOwnerDistributionService {

    @Autowired
    AgentStockItemOwnerDistributionRepository agentStockItemOwnerDistributionRepository;

    @Override
    public AgentStockItemOwnerDistribution registerAgentStockItemOwnerDistribution(AgentStockItemOwnerDistribution agentStockItemOwnerDistribution) {
        return agentStockItemOwnerDistributionRepository.save(agentStockItemOwnerDistribution);
    }

    @Override
    public List<AgentStockItemOwnerDistribution> getAllAgentStockItemOwnerDistributions() {
        return agentStockItemOwnerDistributionRepository.findAll();
    }

    @Override
    public AgentStockItemOwnerDistribution getAgentStockItemOwnerDistributionById(String id) {
        return agentStockItemOwnerDistributionRepository.findStockItemOwnerDistributionById(Integer.parseInt(id));
    }

    @Override
    public AgentStockItemOwnerDistribution updateAgentStockItemOwnerDistributionById(String id, AgentStockItemOwnerDistribution agentStockItemOwnerDistribution) {
        AgentStockItemOwnerDistribution existingAgentStockItemOwnerDistribution = agentStockItemOwnerDistributionRepository.findStockItemOwnerDistributionById(Integer.parseInt(id));
        AgentStockItemOwnerDistribution updatedAgentStockItemDistribution;
        if (existingAgentStockItemOwnerDistribution != null) {
            agentStockItemOwnerDistribution.setId(existingAgentStockItemOwnerDistribution.getId());
            updatedAgentStockItemDistribution = agentStockItemOwnerDistributionRepository.save(agentStockItemOwnerDistribution);
        } else {
            throw new NoSuchElementException("No such agent stock item owner distribution found");
        }
        return updatedAgentStockItemDistribution;
    }

    @Override
    public AgentStockItemOwnerDistribution deleteAgentStockItemOwnerDistributionById(String id) {
        return agentStockItemOwnerDistributionRepository.deleteStockItemOwnerDistributionById(Integer.parseInt(id));
    }
}
