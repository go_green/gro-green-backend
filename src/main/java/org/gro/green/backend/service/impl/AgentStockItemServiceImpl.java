package org.gro.green.backend.service.impl;

import io.swagger.models.auth.In;
import org.gro.green.backend.repository.AgentStockItemRepository;
import org.gro.green.backend.repository.ProductItemRepository;
import org.gro.green.backend.repository.model.AgentStockItem;
import org.gro.green.backend.repository.model.ProductItem;
import org.gro.green.backend.service.AgentStockItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class AgentStockItemServiceImpl implements AgentStockItemService {

    @Autowired
    AgentStockItemRepository agentStockItemRepository;

    @Autowired
    ProductItemRepository productItemRepository;

    @Override
    public AgentStockItem registerAgentStockItem(AgentStockItem agentStockItem) {
        ProductItem productItem = productItemRepository.findProductItemById(agentStockItem.getProductItem().getId());
        agentStockItem.setProductItem(productItem);
        return agentStockItemRepository.save(agentStockItem);
    }

    @Override
    public List<AgentStockItem> getAllAgentStockItems() {
        return agentStockItemRepository.findAll();
    }

    @Override
    public AgentStockItem getAgentStockItemById(String id) {
        return agentStockItemRepository.findAgentStockItemById(Integer.parseInt(id));
    }

    @Override
    public List<AgentStockItem> getAgentStockItemsByAgentId(String id) {
        return agentStockItemRepository.findAgentStockItemsByAgentId(Integer.parseInt(id));
    }

    @Override
    public AgentStockItem updateAgentStockItemById(String id, AgentStockItem agentStockItem) {
        AgentStockItem existingAgentStockItem = agentStockItemRepository.findAgentStockItemById(Integer.parseInt(id));
        AgentStockItem updatedAgentStockItem;
        if (existingAgentStockItem != null) {
            agentStockItem.setId(existingAgentStockItem.getId());
            updatedAgentStockItem = agentStockItemRepository.save(agentStockItem);
        } else {
            throw new NoSuchElementException("No such agent stock item found");
        }
        return updatedAgentStockItem;
    }

    @Override
    public AgentStockItem deleteAgentStockItemById(String id) {
        return agentStockItemRepository.deleteAgentStockItemById(Integer.parseInt(id));
    }
}
