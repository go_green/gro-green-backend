package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.*;
import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.dto.CountAdminDto;
import org.gro.green.backend.repository.model.dto.CountAgentDto;
import org.gro.green.backend.repository.model.dto.DistributionCountDto;
import org.gro.green.backend.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService {

    @Autowired
    ProductItemRepository productItemRepository;

    @Autowired
    FarmerRepository farmerRepository;

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    OwnerDistributionRepository ownerDistributionRepository;

    @Autowired
    AgentDistributionRepository agentDistributionRepository;

    @Autowired
    AgentStockItemDistributionRepository agentStockItemDistributionRepository;

    @Override
    public CountAdminDto getCountForAdmin() {
        DistributionCountDto distributionCountDto = new DistributionCountDto();
        distributionCountDto.setPendingCount(ownerDistributionRepository.getPendingDistributionCount());
        distributionCountDto.setReceivedCount(ownerDistributionRepository.getReceivedDistributionCount());
        distributionCountDto.setRejectedCount(ownerDistributionRepository.getRejectedDistributionCount());

        CountAdminDto countAdminDto = new CountAdminDto();
        countAdminDto.setProductCount(productItemRepository.getProductItemCount());
        countAdminDto.setAgentCount(agentRepository.getAgentCount());
        countAdminDto.setFarmerCount(farmerRepository.getFarmerCount());
        countAdminDto.setDistributionCount(distributionCountDto);

        return countAdminDto;
    }

    @Override
    public CountAgentDto getCountForAgent(Agent agent) {
        DistributionCountDto distributionCountDto = new DistributionCountDto();
        int farmerCount = getFarmerCountByAgent(agent);
        int pendingDistributionCount = getDistributionCountByAgent("pending",agent);
        int receivedDistributionCount = getDistributionCountByAgent("received",agent);
        int rejectedDistributionCount = getDistributionCountByAgent("rejected",agent);
        distributionCountDto.setPendingCount(pendingDistributionCount);
        distributionCountDto.setReceivedCount(receivedDistributionCount);
        distributionCountDto.setRejectedCount(rejectedDistributionCount);
        CountAgentDto countAgentDto = new CountAgentDto();
        countAgentDto.setFarmerCount(farmerCount);
        countAgentDto.setDistributionCount(distributionCountDto);
        return countAgentDto;
    }

    private int getDistributionCountByAgent(String status, Agent agent) {
        return agentStockItemDistributionRepository
                .countAgentStockItemDistributionsByAgentDistribution_StatusAndAgentStockItem_Agent(status,agent);
    }

    private int getFarmerCountByAgent(Agent agent) {
        return farmerRepository.countFarmersByAgent(agent);
    }
}
