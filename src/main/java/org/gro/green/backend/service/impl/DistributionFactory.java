package org.gro.green.backend.service.impl;

import org.gro.green.backend.service.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DistributionFactory {

    @Autowired
    private List<DistributionService> distributionServices;

    private static final Map<String, DistributionService> myServiceCache = new HashMap<>();

    @PostConstruct
    public void initDistributionServiceCache() {
        for(DistributionService service : distributionServices) {
            myServiceCache.put(service.getType(), service);
        }
    }

    public static DistributionService getDistributionService(String type) {
        DistributionService service = myServiceCache.get(type);
        if(service == null) throw new RuntimeException("Unknown service type: " + type);
        return service;
    }

}
