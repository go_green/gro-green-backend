package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.AgentRepository;
import org.gro.green.backend.repository.FarmerRepository;
import org.gro.green.backend.repository.TerritoryRepository;
import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.Territory;
import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.FarmerDto;
import org.gro.green.backend.repository.model.dto.FarmerResDto;
import org.gro.green.backend.service.AgentService;
import org.gro.green.backend.service.FarmerService;
import org.gro.green.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class FarmerServiceImpl implements FarmerService {

    @Autowired
    FarmerRepository farmerRepository;

    @Autowired
    TerritoryRepository territoryRepository;

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    UserService userService;

    @Autowired
    AgentService agentService;

    @Override
    public Farmer saveFarmer(Farmer farmer) {
        return farmerRepository.save(farmer);
    }

    @Override
    public List<Farmer> getAllFarmers() {
        return farmerRepository.findAll();
    }

    @Override
    public List<FarmerResDto> getAllFarmersWithLocations() {
        List<Farmer> allFarmers = farmerRepository.findAll();
        List<FarmerResDto> allFarmerDto = allFarmers.stream()
                .map( f ->
                        new FarmerResDto(
                                f.getUser().getId(),
                                f.getFarmerId(),
                                f.getAgent(),
                                f.getUser().getUsername(),
                                "",
                                f.getFirstName(),
                                f.getLastName(),
                                f.getImageUrl(),
                                f.getUser().getStatus(),
                                territoryRepository.findTerritoriesByFarmerFarmerId(f.getFarmerId()),
                                f.getItemsGrow(),
                                f.getCultivationArea(),
                                f.getMobile(),
                                f.getAddress()
                                )
                ).collect(Collectors.toList());

        return allFarmerDto;
    }

    @Override
    public List<FarmerResDto> getFarmersByAgent(String userId) {
        Agent agent = agentService.getAgentByUserId(userId);
        List<Farmer> allFarmers = farmerRepository.findAllByAgentId(agent.getAgentId());
        List<FarmerResDto> allFarmerDto = allFarmers.stream()
                .map( f ->
                        new FarmerResDto(
                                f.getUser().getId(),
                                f.getFarmerId(),
                                f.getAgent(),
                                f.getUser().getUsername(),
                                "",
                                f.getFirstName(),
                                f.getLastName(),
                                f.getImageUrl(),
                                f.getUser().getStatus(),
                                territoryRepository.findTerritoriesByFarmerFarmerId(f.getFarmerId()),
                                f.getItemsGrow(),
                                f.getCultivationArea(),
                                f.getMobile(),
                                f.getAddress()
                        )
                ).collect(Collectors.toList());

        return allFarmerDto;
    }

    @Override
    public Farmer getFarmerById(String id) {
        return farmerRepository.findFarmerByFarmerId(Integer.parseInt(id));
    }

    @Override
    public Farmer updateFarmer(FarmerDto farmer) {
        Farmer existingFarmer = farmerRepository.findFarmerByFarmerId(farmer.getFarmerId());
        Farmer updatedFarmer;
        if (existingFarmer != null) {
            Agent agent = agentRepository.findAgentByAgentId(farmer.getAgentId());
            User user = existingFarmer.getUser();
            user.setStatus(farmer.getStatus());
            existingFarmer.setUser(user);
            existingFarmer.setAgent(agent);
            existingFarmer.setFirstName(farmer.getFirstName());
            existingFarmer.setLastName(farmer.getLastName());
            existingFarmer.setImageUrl(farmer.getImageUrl());
            existingFarmer.setItemsGrow(farmer.getItemsGrow());
            existingFarmer.setCultivationArea(farmer.getCultivationArea());
            existingFarmer.setMobile(farmer.getMobile());
            existingFarmer.setAddress(farmer.getAddress());
            updatedFarmer = farmerRepository.save(existingFarmer);
        } else {
            throw new NoSuchElementException("Something went wrong");
        }
        return updatedFarmer;
    }

    @Override
    public Farmer deleteFarmerById(String id) {
        return farmerRepository.deleteFarmerByFarmerId(Integer.parseInt(id));
    }

    @Override
    public Farmer registerFarmer(FarmerDto farmerDto) {
        Farmer newFarmer;
        User user = new User(
                0,
                farmerDto.getUsername(),
                farmerDto.getPassword(),
                farmerDto.getStatus(),
                "farmer");
        User newUser = userService.registerUser(user);
        if(newUser != null) {
            Agent agent = agentRepository.findAgentByAgentId(farmerDto.getAgentId());
            Farmer farmer = new Farmer(0,
                    newUser,
                    agent,
                    farmerDto.getFirstName(),
                    farmerDto.getLastName(),
                    farmerDto.getImageUrl(),
                    farmerDto.getItemsGrow(),
                    farmerDto.getCultivationArea(),
                    farmerDto.getMobile(),
                    farmerDto.getAddress());
            newFarmer = saveFarmer(farmer);
            if(farmer != null) {
                farmerDto.getTerritories().forEach((location) -> {
                            Territory territory = new Territory(0,farmer,location.getLat(),location.getLng());
                            territoryRepository.save(territory);
                        }
                        );
            }
        } else {
            throw new NoSuchElementException("No such user create");
        }


        return newFarmer;
    }
}
