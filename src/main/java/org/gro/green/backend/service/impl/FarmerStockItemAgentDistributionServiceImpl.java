package org.gro.green.backend.service.impl;

import io.swagger.models.auth.In;
import org.gro.green.backend.repository.FarmerStockItemAgentDistributionRepository;
import org.gro.green.backend.repository.model.FarmerStockItemAgentDistribution;
import org.gro.green.backend.service.FarmerStockItemAgentDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class FarmerStockItemAgentDistributionServiceImpl implements FarmerStockItemAgentDistributionService {

    @Autowired
    FarmerStockItemAgentDistributionRepository farmerStockItemAgentDistributionRepository;

    @Override
    public FarmerStockItemAgentDistribution registerFarmerStockItemAgentDistribution(FarmerStockItemAgentDistribution farmerStockItemAgentDistribution) {
        return farmerStockItemAgentDistributionRepository.save(farmerStockItemAgentDistribution);
    }

    @Override
    public List<FarmerStockItemAgentDistribution> getAllFarmerStockItemAgentDistributions() {
        return farmerStockItemAgentDistributionRepository.findAll();
    }

    @Override
    public FarmerStockItemAgentDistribution getFarmerStockItemAgentDistributionById(String id) {
        return farmerStockItemAgentDistributionRepository.findFarmerStockItemAgentDistributionById(Integer.parseInt(id));
    }

    @Override
    public FarmerStockItemAgentDistribution updateFarmerStockItemAgentDistributionById(String id, FarmerStockItemAgentDistribution farmerStockItemAgentDistribution) {
        FarmerStockItemAgentDistribution existingFarmerStockItemAgentDistribution = farmerStockItemAgentDistributionRepository.findFarmerStockItemAgentDistributionById(Integer.parseInt(id));
        FarmerStockItemAgentDistribution updatedFarmerStockItemAgentDistribution;
        if (existingFarmerStockItemAgentDistribution != null) {
            farmerStockItemAgentDistribution.setId(existingFarmerStockItemAgentDistribution.getId());
            updatedFarmerStockItemAgentDistribution = farmerStockItemAgentDistributionRepository.save(farmerStockItemAgentDistribution);
        } else {
            throw new NoSuchElementException("No such farmer stock item agent distribution found");
        }
        return updatedFarmerStockItemAgentDistribution;
    }

    @Override
    public FarmerStockItemAgentDistribution deleteFarmerStockItemAgentDistributionById(String id) {
        return farmerStockItemAgentDistributionRepository.deleteFarmerStockItemAgentDistributionById(Integer.parseInt(id));
    }
}
