package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.FarmerStockItemRepository;
import org.gro.green.backend.repository.model.FarmerStockItem;
import org.gro.green.backend.service.FarmerStockItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class FarmerStockItemServiceImpl implements FarmerStockItemService {

    @Autowired
    FarmerStockItemRepository farmerStockItemRepository;

    @Override
    public FarmerStockItem registerFarmerStockItem(FarmerStockItem farmerStockItem) {
        return farmerStockItemRepository.save(farmerStockItem);
    }

    @Override
    public List<FarmerStockItem> getAllFarmerStockItems() {
        return farmerStockItemRepository.findAll();
    }

    @Override
    public FarmerStockItem getFarmerStockItemById(String id) {
        return farmerStockItemRepository.findFarmerStockItemById(Integer.parseInt(id));
    }

    @Override
    public FarmerStockItem updateFarmerStockItemById(String id, FarmerStockItem farmerStockItem) {
        FarmerStockItem existingFarmerStockItem = farmerStockItemRepository.findFarmerStockItemById(Integer.parseInt(id));
        FarmerStockItem updatedFarmerStockItem;
        if (existingFarmerStockItem != null) {
            farmerStockItem.setId(existingFarmerStockItem.getId());
            updatedFarmerStockItem = farmerStockItemRepository.save(farmerStockItem);
        } else {
            throw new NoSuchElementException("No such farmer stock item found");
        }
        return updatedFarmerStockItem;
    }

    @Override
    public FarmerStockItem deleteFarmerStockItemById(String id) {
        return farmerStockItemRepository.deleteFarmerStockItemById(Integer.parseInt(id));
    }
}
