package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.FarmerRepository;
import org.gro.green.backend.repository.FeedbackRepository;
import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.Feedback;
import org.gro.green.backend.repository.model.dto.FeedbackResDto;
import org.gro.green.backend.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    FeedbackRepository feedbackRepository;

    @Autowired
    FarmerRepository farmerRepository;

    @Override
    public Feedback registerFeedback(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }

    @Override
    public List<FeedbackResDto> getAllFeedback() {
        List<Feedback> feedbacks = feedbackRepository.findAll();
        List<FeedbackResDto> feedbackResDtoList =
                feedbacks.stream().map(feedback -> {
                    Farmer farmer = farmerRepository.findFarmerByUser_Id(feedback.getUser().getId());
                    FeedbackResDto feedbackResDto =
                            new FeedbackResDto(
                                    feedback.getFeedbackId(),
                                    feedback.getMessage(),
                                    feedback.getUser(),
                                    farmer
                            );
                    return feedbackResDto;
                }).collect(Collectors.toList());
        return feedbackResDtoList;
    }

    @Override
    public Feedback getFeedbackById(String id) {
        return feedbackRepository.findFeedbackByFeedbackId(Integer.parseInt(id));
    }

    @Override
    public Feedback getFeedbackByUserId(String userId) {
        return feedbackRepository.findFeedbackByUserId(Integer.parseInt(userId));
    }

    @Override
    public Feedback updateFeedbackById(String id, Feedback feedback) {
        Feedback existingFeedback = feedbackRepository.findFeedbackByFeedbackId(Integer.parseInt(id));
        Feedback updatedFeedback;
        if (existingFeedback != null) {
            feedback.setFeedbackId(existingFeedback.getFeedbackId());
            updatedFeedback = feedbackRepository.save(feedback);
        } else {
            throw new NoSuchElementException("No such feedback found");
        }
        return updatedFeedback;
    }

    @Override
    public Feedback deleteFeedbackById(String id) {
        return feedbackRepository.deleteFeedbackByFeedbackId(Integer.parseInt(id));
    }
}
