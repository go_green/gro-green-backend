package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.ImageRepository;
import org.gro.green.backend.repository.model.Image;
import org.gro.green.backend.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    ImageRepository imageRepository;

    @Override
    public Image registerImage(Image image) {
        return imageRepository.save(image);
    }

    @Override
    public List<Image> getAllImages() {
        return imageRepository.findAll();
    }

    @Override
    public Image getImageById(String id) {
        return imageRepository.findImageById(Integer.parseInt(id));
    }

    @Override
    public Image updateImageById(String id, Image image) {
        Image existingImage = imageRepository.findImageById(Integer.parseInt(id));
        Image updatedImage;
        if (existingImage != null) {
            image.setId(existingImage.getId());
            updatedImage = imageRepository.save(image);
        } else {
            throw new NoSuchElementException("No such image found");
        }
        return updatedImage;
    }

    @Override
    public Image deleteImageById(String id) {
        return imageRepository.deleteImageById(Integer.parseInt(id));
    }
}
