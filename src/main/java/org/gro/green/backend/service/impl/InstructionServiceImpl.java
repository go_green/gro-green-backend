package org.gro.green.backend.service.impl;

import io.swagger.models.auth.In;
import org.gro.green.backend.repository.InstructionRepository;
import org.gro.green.backend.repository.model.Instruction;
import org.gro.green.backend.service.InstructionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class InstructionServiceImpl implements InstructionService {

    @Autowired
    InstructionRepository instructionRepository;

    @Override
    public Instruction registerInstruction(Instruction instruction) {
        return instructionRepository.save(instruction);
    }

    @Override
    public List<Instruction> getAllInstructions() {
        return instructionRepository.findAll();
    }

    @Override
    public Instruction getInstructionById(String id) {
        return instructionRepository.findInstructionById(Integer.parseInt(id));
    }

    @Override
    public Instruction updateInstructionById(String id, Instruction instruction) {
        Instruction existingInstruction = instructionRepository.findInstructionById(Integer.parseInt(id));
        Instruction updatedInstruction;
        if (existingInstruction != null) {
            instruction.setId(existingInstruction.getId());
            instruction.setProductItem(existingInstruction.getProductItem());
            updatedInstruction = instructionRepository.save(instruction);
        } else {
            throw new NoSuchElementException("No such instruction found");
        }
        return updatedInstruction;
    }

    @Override
    public Instruction deleteInstructionById(String id) {
        return instructionRepository.deleteInstructionById(Integer.parseInt(id));
    }
}
