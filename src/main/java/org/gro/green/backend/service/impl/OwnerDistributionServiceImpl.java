package org.gro.green.backend.service.impl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.gro.green.backend.repository.*;
import org.gro.green.backend.repository.model.*;
import org.gro.green.backend.repository.model.dto.*;
import org.gro.green.backend.service.DistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Component
public class OwnerDistributionServiceImpl implements DistributionService {

    @Autowired
    OwnerDistributionRepository ownerDistributionRepository;

    @Autowired
    OwnerStockItemsRepository ownerStockItemsRepository;

    @Autowired
    OwnerStockItemDistributionRepository ownerStockItemDistributionRepository;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    FarmerRepository farmerRepository;
    @Autowired
    AgentStockItemRepository agentStockItemRepository;
    @Autowired
    FarmerStockItemRepository farmerStockItemRepository;
    @Autowired
    AgentStockItemOwnerDistributionRepository agentStockItemOwnerDistributionRepository;
    @Autowired
    ProductItemRepository productItemRepository;
    @Autowired
    FarmerStockItemAgentDistributionRepository farmerStockItemAgentDistributionRepository;

    @Override
    public String getType() {
        return "owner";
    }

    @Override
    public DistributionResDto distributeItems(DistributionRequestDto distributionRequestDto) {
        if (checkSenderAvailability(distributionRequestDto.getSenderId())){
            ReceiverAvailability receiverAvailability = checkReceiverAvailability(distributionRequestDto.getReceiverId(), distributionRequestDto.getReceiverRole());
            if (receiverAvailability.getIsAvailable()) {
                Map<Integer, Double> requestedItemsMap = distributionRequestDto.getItems().stream().collect(Collectors.toMap(ItemDto::getStockItemId,ItemDto::getQuantity));
                StockAvailability stockAvailability = checkStockAvailability(distributionRequestDto.getItems(),requestedItemsMap);
                if (stockAvailability.getIsAvailable()) {

                    OwnerDistribution ownerDistribution = saveOwnerDistribution(distributionRequestDto);

                    List <OwnerStockItem> availableStock = stockAvailability.getOwnerStockItems();
                    
                    List<OwnerStockItem> updateOwnerStockItems = updateOwnerStock(availableStock,requestedItemsMap);

                    List<OwnerStockItemDistribution> ownerStockItemDistributions = saveOwnerStockItemDistribution(updateOwnerStockItems, requestedItemsMap, ownerDistribution);

                    arrangeReceiverStock(receiverAvailability,updateOwnerStockItems, ownerDistribution,requestedItemsMap, distributionRequestDto.getReceiverRole());
                    return mapToDistributionResDto(ownerDistribution, ownerStockItemDistributions, receiverAvailability.getReceiver());
                    } else {
                        throw new NoSuchElementException("Stock not available");
                    }


                } else {
                    throw new NoSuchElementException("Receiver not found");
                }
            } else {
                throw new NoSuchElementException("Sender not found");

        }
    }

    @Override
    public List<DistributionResDto> getDistributionList(int id, String receiverRole) {
        if (checkSenderAvailability(id)){
            List<OwnerDistribution> ownerDistributionList = ownerDistributionRepository.findOwnerDistributionsByReceiverRole(receiverRole);
            List<DistributionResDto> distributionResList = ownerDistributionList.stream().map(
                    ownerDistribution -> {
                        List<OwnerStockItemDistribution> ownerStockItemDistributions =
                                ownerStockItemDistributionRepository
                                .findOwnerStockItemDistributionsByOwnerDistributionIdAndOwnerDistributionReceiverRole(ownerDistribution.getId(),receiverRole);
                        Object receiver = new Object();
                        if(receiverRole.equals("agent")) {
                            List<AgentStockItemOwnerDistribution> agentStockItemOwnerDistribution =
                                    agentStockItemOwnerDistributionRepository.findAgentStockItemOwnerDistributionsByOwnerDistributionId(ownerDistribution.getId());
                            if (agentStockItemOwnerDistribution.size() > 0) {
                                Agent receiveAgent = agentStockItemOwnerDistribution.get(0).getAgentStockItem().getAgent();
                                receiver = receiveAgent;
                            }
                        } else {
//                            List<FarmerStockItemAgentDistribution> farmerStockItemAgentDistributions =
//                                    farmerStockItemAgentDistributionRepository.findFarmerStockItemAgentDistributionsByAgentDistribution_Id(ownerDistribution.getId());
//                            if (farmerStockItemAgentDistributions.size() > 0) {
//                                Farmer receiveAgent = farmerStockItemAgentDistributions.get(0).getFarmerStockItem().getFarmer();
//                                receiver = receiveAgent;
//                            }

                        }

                        return mapToDistributionResDto(ownerDistribution, ownerStockItemDistributions, receiver);
                    }
            ).collect(Collectors.toList());
            return distributionResList;
        }
        return null;
    }

    private void arrangeReceiverStock(ReceiverAvailability receiverAvailability, List<OwnerStockItem> updateOwnerStockItems, OwnerDistribution ownerDistribution, Map<Integer, Double> requestedItemsMap, String receiverRole) {
        if (receiverRole.equals("agent")) {
            Agent agent =(Agent) receiverAvailability.getReceiver();
            List<AgentStockItemOwnerDistribution> agentStockItemOwnerDistributions = new ArrayList<>();
            for (OwnerStockItem ownerStockItem:updateOwnerStockItems) {
                ProductItem productItem = new ProductItem(ownerStockItem.getProductItem().getId(),ownerStockItem.getProductItem().getProductName(),
                        ownerStockItem.getProductItem().getDescription(), ownerStockItem.getProductItem().getPoints(), ownerStockItem.getProductItem().getType(),
                        ownerStockItem.getProductItem().getImageUrl(), ownerStockItem.getProductItem().getInstructions());
                List<AgentStockItem> agentStockItem =
                        agentStockItemRepository.findAgentStockItemByAgentAndProductItem(agent, productItem);
                if (agentStockItem.size() > 0) {
                    AgentStockItemOwnerDistribution agentStockItemOwnerDistribution =
                            new AgentStockItemOwnerDistribution(
                                    0,agentStockItem.get(0),ownerDistribution,requestedItemsMap.get(ownerStockItem.getId())
                            );
                    agentStockItemOwnerDistributions.add(agentStockItemOwnerDistribution);
                } else {
                    AgentStockItem newAgentStockItem = agentStockItemRepository.save(
                            new AgentStockItem(0, ownerStockItem.getTitle(), ownerStockItem.getProductItem(), agent,0));
                    AgentStockItemOwnerDistribution agentStockItemOwnerDistribution =
                            new AgentStockItemOwnerDistribution(
                                    0,newAgentStockItem,ownerDistribution,requestedItemsMap.get(ownerStockItem.getId())
                            );
                    agentStockItemOwnerDistributions.add(agentStockItemOwnerDistribution);

                }

            }
            agentStockItemOwnerDistributionRepository.saveAll(agentStockItemOwnerDistributions);
        } else {
            System.out.println("This");
        }

    }

    private DistributionResDto mapToDistributionResDto(OwnerDistribution ownerDistribution, List stockItemDistributions, Object receiver) {
        return new DistributionResDto(
                ownerDistribution.getId(),
                ownerDistribution.getSendDate(),
                ownerDistribution.getReceivedDate(),
                ownerDistribution.getDetail(),
                ownerDistribution.getStatus(),
                stockItemDistributions,
                receiver
                );
    }

    private List<OwnerStockItemDistribution> saveOwnerStockItemDistribution(List<OwnerStockItem> updatedOwnerStockItems, Map<Integer, Double> requestedItemsMap, OwnerDistribution ownerDistribution) {
        List <OwnerStockItemDistribution> ownerStockItemDistributions =
                updatedOwnerStockItems.stream()
                        .map(updatedOwnerStockItem -> {
                            return new OwnerStockItemDistribution(
                                            0,
                                            updatedOwnerStockItem,
                                            ownerDistribution,
                                            requestedItemsMap.get(updatedOwnerStockItem.getId()));
                                }
                        ).collect(Collectors.toList());
        return ownerStockItemDistributionRepository.saveAll(ownerStockItemDistributions);
    }

    private List<OwnerStockItem> updateOwnerStock(List<OwnerStockItem> availableStock, Map<Integer, Double> requestedItemsMap) {
        List <OwnerStockItem> updateStock = availableStock.stream().map(ownerStockItem -> {
            OwnerStockItem newStock = new OwnerStockItem(
                    ownerStockItem.getId(),
                    ownerStockItem.getTitle(),
                    ownerStockItem.getProductItem(),
                    ownerStockItem.getOwner(),
                    ownerStockItem.getQuantity() - requestedItemsMap.get(ownerStockItem.getId())
            );
            return newStock;
        }).collect(Collectors.toList());
        return ownerStockItemsRepository.saveAll(updateStock);
    }

    OwnerDistribution saveOwnerDistribution(DistributionRequestDto distributionRequestDto){
        OwnerDistribution ownerDistribution = new OwnerDistribution(0,
                LocalDateTime.now(),
                null,
                distributionRequestDto.getDetails(),
                distributionRequestDto.getReceiverRole(),
                distributionRequestDto.getStatus());
        return ownerDistributionRepository.save(ownerDistribution);
    }

    @Override
    public Boolean checkSenderAvailability(int id) {
        Owner owner = ownerRepository.findOwnerByOwnerId(id);
        return owner != null;
    }

    public ReceiverAvailability checkReceiverAvailability(int id, String role) {
        if(role.equals("agent")) {
            Agent agent =  agentRepository.findAgentByAgentId(id);
            return new ReceiverAvailability(agent != null, agent);
        } else {
            Farmer farmer = farmerRepository.findFarmerByFarmerId(id);
            return new ReceiverAvailability(farmer != null, farmer);
        }
    }

    public StockAvailability checkStockAvailability(List<ItemDto> items, Map<Integer, Double> requestedItemsMap ) {
        List<OwnerStockItem> ownerStockItems = ownerStockItemsRepository.findAllByIdIn(new ArrayList<>(requestedItemsMap.keySet()));
        List acceptedItemList = ownerStockItems.stream().filter(ownerStockItem ->
            ownerStockItem.getQuantity() >= requestedItemsMap.get(ownerStockItem.getId())
        ).collect(Collectors.toList());
        return new StockAvailability(acceptedItemList.size() == items.size(), ownerStockItems);
    }

    @Override
    public List<DistributionResDto> getPendingDistribution(DistributionListReqDto distributionListReqDto) {
        ReceiverAvailability receiverAvailability = checkReceiverAvailability(distributionListReqDto.getReceiverId(), distributionListReqDto.getReceiverRole());
        if(receiverAvailability.getIsAvailable()) {
            if (distributionListReqDto.getReceiverRole().equals("agent")){
                Agent agent =(Agent) receiverAvailability.getReceiver();
                try{
                    List<AgentStockItemOwnerDistribution> agentStockItems =
                            agentStockItemOwnerDistributionRepository
                                    .findAgentStockItemOwnerDistributionsByAgentStockItem_AgentAndOwnerDistributionStatus(
                                            agent,
                                            distributionListReqDto.getStatus()
                                    );
                    Map<OwnerDistribution, List<AgentStockItemOwnerDistribution>> agentStockItemOwnerDistributions = agentStockItems.stream()
                            .collect(Collectors.groupingBy(AgentStockItemOwnerDistribution::getOwnerDistribution));
                    List<DistributionResDto> distributionResDtos = new ArrayList<>();
                    for(Map.Entry<OwnerDistribution, List<AgentStockItemOwnerDistribution>> agentStockItemOwnerDistribution:agentStockItemOwnerDistributions.entrySet() ){
                        distributionResDtos.add(
                                new DistributionResDto<AgentStockItemOwnerDistribution>(
                                        agentStockItemOwnerDistribution.getKey().getId(),
                                        agentStockItemOwnerDistribution.getKey().getSendDate(),
                                        agentStockItemOwnerDistribution.getKey().getReceivedDate(),
                                        agentStockItemOwnerDistribution.getKey().getDetail(),
                                        agentStockItemOwnerDistribution.getKey().getStatus(),
                                        agentStockItemOwnerDistribution.getValue(),
                                        receiverAvailability.getReceiver()
                                )
                        );
                    }
                    return distributionResDtos;
                } catch (Exception e) {
                    throw new NoSuchElementException("Persisting error");
                }

            }
//            ownerDistributionRepository.()
        } else {
            throw new NoSuchElementException("Receiver not found");
        }
        throw new NoSuchElementException("Something went wrong");
    }

    @Override
    public DistributionResDto updateStatus(DistributionStatusUpdateReqDto distributionStatusUpdateReqDto) {
        if (distributionStatusUpdateReqDto.getStatus().equals("received")) {
            if (distributionStatusUpdateReqDto.getReceiverRole().equals("agent")) {
                List<AgentStockItemOwnerDistribution> agentStockItemOwnerDistributions
                        = agentStockItemOwnerDistributionRepository
                        .findAgentStockItemOwnerDistributionsByOwnerDistributionId(distributionStatusUpdateReqDto.getDistributionId());
                List<AgentStockItem> updatedAgentStockItem = updateAgentStock(agentStockItemOwnerDistributions);
                OwnerDistribution updatedOwnerDistribution = updateOwnerDistribution(distributionStatusUpdateReqDto);
                return mapToDistributionResDto(updatedOwnerDistribution,updatedAgentStockItem,null);
            } else {

            }

        } else if (distributionStatusUpdateReqDto.getStatus().equals("rejected")) {
            if (distributionStatusUpdateReqDto.getReceiverRole().equals("agent")) {
                List<OwnerStockItemDistribution> agentStockItemOwnerDistributions
                            = ownerStockItemDistributionRepository
                            .findOwnerStockItemDistributionsByOwnerDistributionIdAndOwnerDistributionReceiverRole(
                                    distributionStatusUpdateReqDto.getDistributionId(),
                                    distributionStatusUpdateReqDto.getReceiverRole());


                List<OwnerStockItem> updatedAgentStockItem = updateOwnerStockWhenRejected(agentStockItemOwnerDistributions);
                OwnerDistribution updatedOwnerDistribution = updateOwnerDistribution(distributionStatusUpdateReqDto);
                return mapToDistributionResDto(updatedOwnerDistribution,updatedAgentStockItem, null);
            } else {

            }

        }
        return null;
    }

    private List<OwnerStockItem> updateOwnerStockWhenRejected(List<OwnerStockItemDistribution> ownerStockItemOwnerDistributions) {
        List<OwnerStockItem> updatedOwnerStock = ownerStockItemOwnerDistributions.stream()
                .map(ownerStockItemDistribution -> {
                    OwnerStockItem ownerStockItem = ownerStockItemDistribution.getOwnerStockItem();
                    ownerStockItem.setQuantity(ownerStockItem.getQuantity() + ownerStockItemDistribution.getQty());
                    return ownerStockItem;
                }
                ).collect(Collectors.toList());
        return ownerStockItemsRepository.saveAll(updatedOwnerStock);
    }

    private OwnerDistribution updateOwnerDistribution(DistributionStatusUpdateReqDto distribution) {
        OwnerDistribution ownerDistribution = ownerDistributionRepository.findOwnerDistributionById(distribution.getDistributionId());
        ownerDistribution.setStatus(distribution.getStatus());
        ownerDistribution.setReceivedDate(LocalDateTime.now());
        return ownerDistributionRepository.save(ownerDistribution);

    }

    private List<AgentStockItem> updateAgentStock(List<AgentStockItemOwnerDistribution> agentStockItemOwnerDistributions) {
        List<AgentStockItem> updatedAgentStock = agentStockItemOwnerDistributions.stream()
                .map(agentStockItemOwnerDistribution -> {
                    AgentStockItem newAgentStockItem = agentStockItemOwnerDistribution.getAgentStockItem();
                    newAgentStockItem.setQuantity(newAgentStockItem.getQuantity() + agentStockItemOwnerDistribution.getQty());
                    return newAgentStockItem;
                }
                ).collect(Collectors.toList());
        return agentStockItemRepository.saveAll(updatedAgentStock);
    }
}

@Getter
@Setter
@AllArgsConstructor
class StockAvailability{
    Boolean isAvailable;
    List<OwnerStockItem> ownerStockItems;
}

@Getter
@Setter
@AllArgsConstructor
class ReceiverAvailability<T>{
    Boolean isAvailable;
    T receiver;
}


