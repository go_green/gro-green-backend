package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.OwnerRepository;
import org.gro.green.backend.repository.model.Owner;
import org.gro.green.backend.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OwnerServiceImpl implements OwnerService {

    @Autowired
    OwnerRepository ownerRepository;

    @Override
    public Owner registerOwner(Owner owner) {
        return ownerRepository.save(owner);
    }

    @Override
    public List<Owner> getAllOwners() {
        return ownerRepository.findAll();
    }

    @Override
    public Owner getOwnerById(String id) {
        return ownerRepository.findOwnerByOwnerId(Integer.parseInt(id));
    }

    @Override
    public Owner getOwnerByUserId(String userId) {
        return ownerRepository.findOwnerByUserId(Integer.parseInt(userId));
    }

    @Override
    public Owner updateOwnerById(String id, Owner owner) {
        Owner existingOwner = ownerRepository.findOwnerByOwnerId(Integer.parseInt(id));
        Owner updatedOwner;
        if (existingOwner != null) {
            owner.setOwnerId(existingOwner.getOwnerId());
            updatedOwner = ownerRepository.save(owner);
        } else {
            throw new NoSuchElementException("No such owner found");
        }
        return updatedOwner;
    }

    @Override
    public Owner deleteOwnerById(String id) {
        return ownerRepository.deleteOwnerByOwnerId(Integer.parseInt(id));
    }
}
