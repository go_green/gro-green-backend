package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.OwnerStockItemDistributionRepository;
import org.gro.green.backend.repository.model.OwnerStockItemDistribution;
import org.gro.green.backend.service.OwnerStockItemDistributionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OwnerStockItemDistributionServiceImpl implements OwnerStockItemDistributionService {

    @Autowired
    OwnerStockItemDistributionRepository ownerStockItemDistributionRepository;

    @Override
    public OwnerStockItemDistribution registerOwnerStockItemDistribution(OwnerStockItemDistribution ownerStockItemDistribution) {
        return ownerStockItemDistributionRepository.save(ownerStockItemDistribution);
    }

    @Override
    public List<OwnerStockItemDistribution> getAllOwnerStockItemDistributions() {
        return ownerStockItemDistributionRepository.findAll();
    }

    @Override
    public OwnerStockItemDistribution getOwnerStockItemDistributionById(String id) {
        return ownerStockItemDistributionRepository.findOwnerStockItemDistributionById(Integer.parseInt(id));
    }

    @Override
    public OwnerStockItemDistribution updateOwnerStockItemDistributionById(String id, OwnerStockItemDistribution ownerStockItemDistribution) {
        OwnerStockItemDistribution existingOwner = ownerStockItemDistributionRepository.findOwnerStockItemDistributionById(Integer.parseInt(id));
        OwnerStockItemDistribution updatedOwner;
        if (existingOwner != null) {
            ownerStockItemDistribution.setId(existingOwner.getId());
            updatedOwner = ownerStockItemDistributionRepository.save(ownerStockItemDistribution);
        } else {
            throw new NoSuchElementException("No such owner stock items distribution found");
        }
        return updatedOwner;
    }

    @Override
    public OwnerStockItemDistribution deleteOwnerStockItemDistributionById(String id) {
        return ownerStockItemDistributionRepository.deleteOwnerStockItemDistributionById(Integer.parseInt(id));
    }
}
