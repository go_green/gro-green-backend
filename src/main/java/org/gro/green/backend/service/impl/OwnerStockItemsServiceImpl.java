package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.OwnerStockItemsRepository;
import org.gro.green.backend.repository.ProductItemRepository;
import org.gro.green.backend.repository.model.OwnerStockItem;
import org.gro.green.backend.repository.model.ProductItem;
import org.gro.green.backend.service.OwnerStockItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class OwnerStockItemsServiceImpl implements OwnerStockItemsService {

    @Autowired
    OwnerStockItemsRepository ownerStockItemsRepository;

    @Autowired
    ProductItemRepository productItemRepository;

    @Override
    public OwnerStockItem registerOwnerStockItems(OwnerStockItem ownerStockItem) {
        ProductItem productItem = productItemRepository.findProductItemById(ownerStockItem.getProductItem().getId());
        ownerStockItem.setProductItem(productItem);
        return ownerStockItemsRepository.save(ownerStockItem);
    }

    @Override
    public List<OwnerStockItem> getAllOwnerStockItems() {
        return ownerStockItemsRepository.findAll();
    }

    @Override
    public OwnerStockItem getOwnerStockItemsById(String id) {
        return ownerStockItemsRepository.findOwnerStockItemsById(Integer.parseInt(id));
    }

    @Override
    public OwnerStockItem updateOwnerStockItemsById(String id, OwnerStockItem ownerStockItem) {
        OwnerStockItem existingOwnerStockItem = ownerStockItemsRepository.findOwnerStockItemsById(Integer.parseInt(id));
        OwnerStockItem updatedOwnerStockItem;
        if (existingOwnerStockItem != null) {
            ownerStockItem.setId(existingOwnerStockItem.getId());
            updatedOwnerStockItem = ownerStockItemsRepository.save(ownerStockItem);
        } else {
            throw new NoSuchElementException("No such owner stock items found");
        }
        return updatedOwnerStockItem;
    }

    @Override
    public OwnerStockItem deleteOwnerStockItemsById(String id) {
        return ownerStockItemsRepository.deleteOwnerStockItemsById(Integer.parseInt(id));
    }
}
