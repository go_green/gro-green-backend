package org.gro.green.backend.service.impl;

import io.swagger.models.auth.In;
import org.gro.green.backend.repository.InstructionRepository;
import org.gro.green.backend.repository.ProductItemRepository;
import org.gro.green.backend.repository.model.Instruction;
import org.gro.green.backend.repository.model.ProductItem;
import org.gro.green.backend.service.InstructionService;
import org.gro.green.backend.service.ProductItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class ProductItemServiceImpl implements ProductItemService {

    @Autowired
    ProductItemRepository productItemRepository;

    @Autowired
    InstructionService instructionService;

    @Override
    public ProductItem registerProductItem(ProductItem productItem) {
        ProductItem newProductItem = productItemRepository.save(productItem);
        if(productItem.getInstructions() != null) {
            productItem.getInstructions().forEach(instruction -> {
                instruction.setProductItem(productItem);
                instructionService.updateInstructionById(String.valueOf(instruction.getId()), instruction);
            });
        }
        return newProductItem;
    }

    @Override
    public List<ProductItem> getAllProductItems() {
        return productItemRepository.findAll();
    }

    @Override
    public ProductItem getProductItemById(String id) {
        return productItemRepository.findProductItemById(Integer.parseInt(id));
    }

    @Override
    public ProductItem updateProductItemById(String id, ProductItem productItem) {
        ProductItem existingProductItem = productItemRepository.findProductItemById(Integer.parseInt(id));
        ProductItem updatedProductItem;
        if (existingProductItem != null) {
            if((productItem.getInstructions() != null) && (productItem.getInstructions().size() != 0)) {
                if(productItem.getInstructions().size() > existingProductItem.getInstructions().size()) {
                    productItem.getInstructions().forEach(instruction -> {
                        if(this.instructionService.getInstructionById(String.valueOf(instruction.getId())) == null) {
                            Instruction newInstruction = this.instructionService.registerInstruction(instruction);
                            newInstruction.setProductItem(existingProductItem);
                            this.instructionService.updateInstructionById(String.valueOf(newInstruction.getId()), newInstruction);
                        }
                    });
                }
                productItem.getInstructions().forEach(instruction -> {
                    instructionService.updateInstructionById(String.valueOf(instruction.getId()), instruction);
                });
            }
            productItem.setId(existingProductItem.getId());
            updatedProductItem = productItemRepository.save(productItem);
        } else {
            throw new NoSuchElementException("No such production item found");
        }
        return updatedProductItem;
    }

    @Override
    public ProductItem deleteProductItemById(String id) {
        return productItemRepository.deleteProductItemById(Integer.parseInt(id));
    }
}
