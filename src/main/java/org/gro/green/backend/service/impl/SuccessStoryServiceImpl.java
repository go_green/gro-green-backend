package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.SuccessStoryRepository;
import org.gro.green.backend.repository.model.SuccessStory;
import org.gro.green.backend.service.SuccessStoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class SuccessStoryServiceImpl implements SuccessStoryService {

    @Autowired
    SuccessStoryRepository successStoryRepository;

    @Override
    public SuccessStory registerSuccessStory(SuccessStory SuccessStory) {
        return successStoryRepository.save(SuccessStory);
    }

    @Override
    public List<SuccessStory> getAllSuccessStorys() {
        return successStoryRepository.findAll();
    }

    @Override
    public SuccessStory getSuccessStoryById(String id) {
        return successStoryRepository.findSuccessStoriesById(Integer.parseInt(id));
    }

    @Override
    public SuccessStory updateSuccessStoryById(String id, SuccessStory SuccessStory) {
        SuccessStory existingSuccessStory = successStoryRepository.findSuccessStoriesById(Integer.parseInt(id));
        SuccessStory updatedSuccessStory;
        if (existingSuccessStory != null) {
            SuccessStory.setId(existingSuccessStory.getId());
            updatedSuccessStory = successStoryRepository.save(SuccessStory);
        } else {
            throw new NoSuchElementException("No such success story found");
        }
        return updatedSuccessStory;
    }

    @Override
    public SuccessStory deleteSuccessStoryById(String id) {
        return successStoryRepository.deleteSuccessStoriesById(Integer.parseInt(id));
    }
}
