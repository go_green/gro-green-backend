package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.TerritoryRepository;
import org.gro.green.backend.repository.model.Territory;
import org.gro.green.backend.service.TerritoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class TerritoryServiceImpl implements TerritoryService {

    @Autowired
    TerritoryRepository territoryRepository;

    @Override
    public Territory registerTerritory(Territory territory) {
        return territoryRepository.save(territory);
    }

    @Override
    public List<Territory> getAllTerritorys() {
        return territoryRepository.findAll();
    }

    @Override
    public Territory getTerritoryById(String id) {
        return territoryRepository.findTerritoriesByLocationId(Integer.parseInt(id));
    }

    @Override
    public Territory updateTerritoryById(String id, Territory territory) {
        Territory existingTerritory = territoryRepository.findTerritoriesByLocationId(Integer.parseInt(id));
        Territory updatedTerritory;
        if (existingTerritory != null) {
            territory.setLocationId(existingTerritory.getLocationId());
            updatedTerritory = territoryRepository.save(territory);
        } else {
            throw new NoSuchElementException("No such territory found");
        }
        return updatedTerritory;
    }

    @Override
    public Territory deleteTerritoryById(String id) {
        return territoryRepository.deleteTerritoriesByLocationId(Integer.parseInt(id));
    }

    @Override
    public List<Territory> getTerritoriesByAgentId(int agentId) {
        return territoryRepository.findByAgentId(agentId);
    }
}
