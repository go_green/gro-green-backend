package org.gro.green.backend.service.impl;

import org.gro.green.backend.repository.AgentRepository;
import org.gro.green.backend.repository.FarmerRepository;
import org.gro.green.backend.repository.OwnerRepository;
import org.gro.green.backend.repository.UserRepository;
import org.gro.green.backend.repository.model.Agent;
import org.gro.green.backend.repository.model.Farmer;
import org.gro.green.backend.repository.model.Owner;
import org.gro.green.backend.repository.model.User;
import org.gro.green.backend.repository.model.dto.ChangePasswordRequestDto;
import org.gro.green.backend.repository.model.dto.UserResponseDto;
import org.gro.green.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder bcryptEncoder;

    @Autowired
    OwnerRepository ownerRepository;

    @Autowired
    AgentRepository agentRepository;

    @Autowired
    FarmerRepository farmerRepository;

    @Override
    public User registerUser(User user) {
        String bCryptPassword = bcryptEncoder.encode(user.getPassword());
        user.setPassword(bCryptPassword);
        return userRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(String Id) {
        return userRepository.findUserById(Integer.parseInt(Id));
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    @Override
    public User deleteUserById(String Id) {
        return userRepository.deleteUserById(Integer.parseInt(Id));
    }

    @Override
    public User updateUserById(String Id, User user) {
        User existingUser = userRepository.findUserById(Integer.parseInt(Id));
        User updatedUser = new User();
        if (existingUser != null) {
            user.setId(existingUser.getId());
//            String bCryptPassword = bcryptEncoder.encode(user.getPassword());
//            user.setPassword(bCryptPassword);
            updatedUser = userRepository.save(user);
        }
        else {
            try {
                throw new NoSuchFieldException();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return updatedUser;
    }

    @Override
    public User updateUserStatusById(User userNewDetails) {
        User existingUser = userRepository.findUserById(userNewDetails.getId());
        User updatedUser = new User();
        if (existingUser != null) {
            if (userNewDetails.getStatus().equals("deleted")) {
                existingUser.setStatus("deleted");
            } else if (userNewDetails.getStatus().equals("active")) {
                existingUser.setStatus("active");
            }
            updatedUser = userRepository.save(existingUser);
        }
        else {
            try {
                throw new NoSuchFieldException();
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
        return updatedUser;
    }

    @Override
    public UserResponseDto getUserWithUserIdByUsername(String username) {
        User user = userRepository.findUserByUsername(username);
        UserResponseDto userResponseDto;
        if (user.getRole().equals("owner")) {
            Owner owner = ownerRepository.findOwnerByUserId(user.getId());
            userResponseDto = new UserResponseDto(
                    user.getId(),
                    user.getUsername(),
                    user.getStatus(),
                    user.getRole(),
                    owner.getOwnerId()
                    );
        } else if (user.getRole().equals("agent")) {
            Agent agent =  agentRepository.findAgentByUserId(user.getId());
            userResponseDto = new UserResponseDto(
                    user.getId(),
                    user.getUsername(),
                    user.getStatus(),
                    user.getRole(),
                    agent.getAgentId()
            );
        } else {
            Farmer farmer = farmerRepository.findFarmerByUser_Id(user.getId());
            userResponseDto = new UserResponseDto(
                    user.getId(),
                    user.getUsername(),
                    user.getStatus(),
                    user.getRole(),
                    farmer.getFarmerId()
            );
        }
        return userResponseDto;
    }

    @Override
    public User updateUserPasswordById(int userId, ChangePasswordRequestDto changePasswordRequest) {
        User user = userRepository.findUserById(userId);
        if (user != null) {
            String bCryptPassword = bcryptEncoder.encode(changePasswordRequest.getNewPassword());
            user.setPassword(bCryptPassword);
            user = userRepository.save(user);
        } else {
            throw new UsernameNotFoundException("User not found with userId: " + userId);
        }
        return user;
    }
}
